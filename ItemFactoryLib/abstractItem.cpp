
#include "abstractItem.h"
#include "memPlugins.h"

#include "utils\stringUtils.h"

#include <algorithm>
#include <iterator>

AbstractItem::AbstractItem(std::shared_ptr<MemPlugins> plugins)
	:m_plugins(plugins)
{
	m_tableSize = 0;
	m_baseAddr = 0;
}

AbstractItem::~AbstractItem()	//@brief. �Ҹ���
{ }

void AbstractItem::AppendItemData(const std::vector<uint8_t> &fieldKeyV, itemDetailType &details)
{
	std::string itemname;

	bool cond = true;
	std::copy_if(fieldKeyV.begin(), fieldKeyV.end(), std::insert_iterator<std::string>(itemname, itemname.begin()),
		[&cond](uint8_t chk)->bool { if (!chk) cond = false; return cond; });
	m_itemData.emplace(itemname, details);
	m_thingIdKey.emplace(details.unitThingId, itemname);
}

uint32_t AbstractItem::SetThingIdOffset()
{
	return 4;
}

CString AbstractItem::GetDescription(uint32_t baseAddr)
{
	std::shared_ptr<MemPlugins> plugins(m_plugins);
	uint32_t descAddr = plugins->GetMemory<uint32_t>(baseAddr + 8);
	CString desc;

	if (descAddr)
	{
		std::vector<uint16_t> readV(48);

		plugins->ReadStream<uint16_t>(descAddr, readV);
		auto readerIterator = readV.begin();

		while (readerIterator != readV.end())
		{
			desc.AppendChar(*readerIterator);
			++readerIterator;
		}
	}
	return desc;
}

bool AbstractItem::ReadTable(uint32_t addr)
{
	std::shared_ptr<MemPlugins> plugins(m_plugins);
	uint32_t curAddr = addr;
	uint32_t nextAddr = 0;

	if (!plugins || !m_tableSize)
		return false;
	curAddr = plugins->GetMemory<uint32_t>(curAddr);
	while (curAddr)
	{
		itemDetailType details{};
		std::vector<uint8_t> destV(m_tableSize);
		uint32_t fieldNameAddr = plugins->GetMemory<uint32_t>(curAddr);

		if (!fieldNameAddr)
			break;
		std::vector<uint8_t> fieldKeyV(24);
		plugins->ReadStream<uint8_t>(fieldNameAddr, fieldKeyV);

		plugins->ReadStream<uint8_t>(curAddr, destV);
		*reinterpret_cast<uint32_t *>(&destV[0]) = curAddr;
		details.unitThingId = *reinterpret_cast<uint32_t *>(&destV[m_thingIdOffset]);

		details.tableOffset = curAddr;
		details.rawData = destV;
		details.itemDesc = GetDescription(curAddr);

		AppendItemData(fieldKeyV, details);
		curAddr = plugins->GetMemory<uint32_t>(curAddr + m_tableSize);
	}
	return false;
}

uint32_t AbstractItem::GetThingID(const std::string &itemname)
{
	auto keyIterator = m_itemData.find(itemname);

	if (keyIterator != m_itemData.end())
	{
		return *reinterpret_cast<uint32_t *>(&keyIterator->second.rawData[m_thingIdOffset]);
	}
	return -1;
}

bool AbstractItem::DoInitialize(bool reset)
{
	if (m_plugins.expired())
		return false;

	if (reset)
	{
		if (m_itemData.size())
			m_itemData.clear();
	}
	m_tableSize = SetTableSize();
	m_baseAddr = SetBaseAddress();
	m_thingIdOffset = SetThingIdOffset();
	return ReadTable(m_baseAddr);
}

int AbstractItem::ItemTypeNumber() const
{
	return typeNumber::TYPE;
}

std::list<std::string> AbstractItem::GetKeyList()
{
	std::list<std::string> keylist;

	std::transform(m_itemData.begin(), m_itemData.end(), std::insert_iterator<std::list<std::string>>(keylist, keylist.begin()),
		[](auto node)->std::string { return node.first; });
	return keylist;
}

uint32_t AbstractItem::GetItemAddr(const std::string &key)
{
	auto keyIterator = m_itemData.find(key);

	if (keyIterator != m_itemData.end())
		return keyIterator->second.tableOffset;
	return 0;
}

std::string AbstractItem::GetItemDesc(const std::string &key)
{
	auto keyIterator = m_itemData.find(key);

	if (keyIterator != m_itemData.end())
	{
		std::wstring ws(keyIterator->second.itemDesc.operator LPCWSTR());
		std::string out;

		StringUtils::UnicodeToAnsi(ws, out);
		return out;
	}
	return{};
}

std::string AbstractItem::FindKeyWithThingId(uint32_t thingId)
{
	auto idIterator = m_thingIdKey.find(thingId);

	if (idIterator != m_thingIdKey.end())
		return idIterator->second;
	return std::string();
}