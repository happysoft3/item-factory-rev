
#ifndef ABSTRACT_ITEM_H__
#define ABSTRACT_ITEM_H__

#include <memory>
#include <map>
#include <vector>
#include <atlstr.h>
#include <list>

using itemDetailType = struct
{
	uint32_t tableOffset;
	uint32_t unitThingId;
	std::vector<uint8_t> rawData;
	CString itemDesc;
};

class MemPlugins;

//@brief. 아이템객체를 통칭하는 추상클래스 입니다
class AbstractItem
{
private:
	std::weak_ptr<MemPlugins> m_plugins;
	uint32_t m_tableSize;
	std::map<std::string, itemDetailType> m_itemData;
	uint32_t m_baseAddr;
	uint32_t m_thingIdOffset;

	std::map<uint32_t, std::string> m_thingIdKey;

	enum typeNumber { TYPE = 0 };

public:
	explicit AbstractItem(std::shared_ptr<MemPlugins> plugins);		//@brief. 반드시 메모리 플러그인 객체를 소유해야 합니다
	virtual ~AbstractItem();

private:
	void AppendItemData(const std::vector<uint8_t> &fieldKeyV, itemDetailType &details);
	virtual uint32_t SetTableSize() = 0;
	virtual uint32_t SetBaseAddress() = 0;
	virtual uint32_t SetThingIdOffset();
	CString GetDescription(uint32_t baseAddr);
	bool ReadTable(uint32_t addr);		//@brief. read 80 bytes

public:
	uint32_t GetThingID(const std::string &itemname);
	virtual bool DoInitialize(bool reset = false);
	virtual int ItemTypeNumber() const;
	std::list<std::string> GetKeyList();
	uint32_t GetItemAddr(const std::string &key);
	std::string GetItemDesc(const std::string &key);

	std::string FindKeyWithThingId(uint32_t thingId);
};

#endif