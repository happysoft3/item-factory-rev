
#include "armors.h"
#include "memPlugins.h"


Armors::Armors(std::shared_ptr<MemPlugins> plugins)
	: AbstractItem(plugins)
{ }

Armors::~Armors()
{ }

uint32_t Armors::SetBaseAddress()
{
	return 0x611C6C;
}

uint32_t Armors::SetTableSize()
{
	return 80;
}

//@brief. 초기화 작업이 진행될 때, 메모리로 부터 데이터를 로드해 옵니다
bool Armors::DoInitialize(bool reset)
{
	//Todo. 초기화 작업이 필요하다면 여기에서
	return AbstractItem::DoInitialize(reset);
}

int Armors::ItemTypeNumber() const
{
	return TypeId::Type;
}