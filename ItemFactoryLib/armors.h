
#ifndef ARMORS_H__
#define ARMORS_H__

#include "abstractItem.h"

//@brief. ����
class Armors : public AbstractItem
{
private:
	enum TypeId { Type = 1 };

public:
	explicit Armors(std::shared_ptr<MemPlugins> plugins);
	virtual ~Armors() override;

private:
	virtual uint32_t SetBaseAddress() override;
	virtual uint32_t SetTableSize() override;

public:
	virtual bool DoInitialize(bool reset = false) override;
	virtual int ItemTypeNumber() const override;
};

#endif