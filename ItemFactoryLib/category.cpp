
#define MAKEDLL
#include "category.h"
#include <map>
#include <vector>
#include <random>
#include <algorithm>
#include <iterator>

class Category::CategoryPimpl
{
private:
	std::mt19937 rGenerator;
	std::random_device rndDevice;
	std::uniform_int_distribution<std::mt19937::result_type> rndRange;

public:
	std::map<std::string, std::vector<std::string>> node;

public:
	CategoryPimpl()
		: rndDevice(),
		rndRange(std::uniform_int_distribution<std::mt19937::result_type>(0, 0x100000))
	{
		rGenerator = std::mt19937(rndDevice());
	}

	int GetRandom()
	{
		return rndRange(rGenerator);
	}
};

Category::Category()
	: m_pimpl(new CategoryPimpl)
{ }

Category::~Category()
{ }

void Category::InsertNewItem(const std::string &key, const std::string &itemname)
{
	auto keyIterator = m_pimpl->node.find(key);

	if (keyIterator == m_pimpl->node.end())
		m_pimpl->node.emplace(key, std::vector<std::string>({itemname}));
	else
		keyIterator->second.push_back(itemname);
}

void Category::InsertNewItem(const std::string &key, const std::initializer_list<std::string> &itemlist)
{
	auto keyIterator = m_pimpl->node.find(key);

	if (keyIterator == m_pimpl->node.end())
		m_pimpl->node.emplace(key, itemlist);
	else
		std::transform(itemlist.begin(), itemlist.end(), keyIterator->second.end(), [](const std::string &value) { return value; });
}

std::string Category::PickupItemAtRandom(const std::string &key)
{
	auto keyIterator = m_pimpl->node.find(key);

	if (keyIterator != m_pimpl->node.end())
	{
		if (keyIterator->second.size())
		{
			int rnd = m_pimpl->GetRandom() % keyIterator->second.size();

			return keyIterator->second[rnd];
		}
	}
	return{};
}