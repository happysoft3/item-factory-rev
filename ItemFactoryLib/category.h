
#ifndef CATEGORY_H__
#define CATEGORY_H__

#ifdef MAKEDLL
#define EXPORT_OBJECT __declspec(dllexport)
#else
#define EXPORT_OBJECT __declspec(dllimport)
#endif

#include <memory>
#include <string>

class EXPORT_OBJECT Category
{
private:
	class CategoryPimpl;
	std::unique_ptr<CategoryPimpl> m_pimpl;

public:
	explicit Category();
	~Category();
	void InsertNewItem(const std::string &key, const std::string &itemname);
	void InsertNewItem(const std::string &key, const std::initializer_list<std::string> &itemlist);
	std::string PickupItemAtRandom(const std::string &key);

};

#endif