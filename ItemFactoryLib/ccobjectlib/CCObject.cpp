#define MAKEDLL
#include "CCObject.h"
#include "GenericRoot.h"

#include <map>
#include <tuple>
#include <algorithm>
#include <list>


using clientElement = std::tuple<std::weak_ptr<CCObject::CCObjectPimpl>, CCObject::slotMethodType>;
using clientMapType = std::map<uint32_t, clientElement>;

uint32_t CCObject::m_latestObjectId = 0;

class CCObject::CCObjectPimpl
{
public:
	CCObjectPimpl();
	~CCObjectPimpl();

	CCObject *parent;
	//@brief. public variables
	std::map<uint32_t, clientMapType> clientNode;
};

CCObject::CCObjectPimpl::CCObjectPimpl()
{ }

CCObject::CCObjectPimpl::~CCObjectPimpl()
{ }

CCObject::CCObject()
	: m_impl(std::make_shared<CCObjectPimpl>())
{
	m_impl->parent = this;
	m_objectId = m_latestObjectId++;
}

CCObject::~CCObject()
{ }

//@usage. serverObject.connect(signal, me, slot)
bool CCObject::Connection(uint32_t signalId, CCObject *receiver, slotMethodType slotMethod)
{
	if (receiver == nullptr)
		return false;

	auto keyIterator = m_impl->clientNode.find(signalId);

	if (keyIterator == m_impl->clientNode.end())
	{
		clientMapType clients;

		clients.emplace(receiver->ObjectId(), std::make_tuple(receiver->m_impl, slotMethod));
		m_impl->clientNode[signalId] = clients;	//regist a new signal
	}
	else
	{
		clientMapType &clients = keyIterator->second;

		clients.emplace(receiver->ObjectId(), std::make_tuple(receiver->m_impl, slotMethod));
	}
	OnNewConnect(receiver);
	return true;
}

void CCObject::Disconnection(CCObject *receiver, uint32_t signalId)
{
	bool disconnectAll = signalId == -1;
	auto keyIterator = disconnectAll ? m_impl->clientNode.begin() : m_impl->clientNode.find(signalId);

	if (receiver == nullptr)
		return;
	while (keyIterator != m_impl->clientNode.end())
	{
		auto subkeyIterator = keyIterator->second.find(receiver->ObjectId());

		if (subkeyIterator != keyIterator->second.end())
			keyIterator->second.erase(subkeyIterator);

		if (disconnectAll)
			++keyIterator;
		else
			break;
	}
}

void CCObject::Emit(uint32_t signalId, std::shared_ptr<GenericRoot> messageData)
{
	auto keyIterator = m_impl->clientNode.find(signalId);

	if (keyIterator == m_impl->clientNode.end())
		return;
	std::list<clientMapType::iterator> invalidIteratorList;

	clientMapType::iterator clientIterator = keyIterator->second.begin();
	while (clientIterator != keyIterator->second.end())
	{
		if (std::get<0>(clientIterator->second).expired())
			invalidIteratorList.push_back(clientIterator);
		else
		{
			CCObject *client = std::get<0>(clientIterator->second).lock()->parent;

			std::get<1>(clientIterator->second)(client, messageData);
		}
		++clientIterator;
	}

	clientMapType &clientsRef = keyIterator->second;
	std::for_each(invalidIteratorList.begin(), invalidIteratorList.end(), [&clientsRef](auto element)
	{
		clientsRef.erase(element);
	});
}
