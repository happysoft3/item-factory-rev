
#ifndef CC_OBJECT_H__
#define CC_OBJECT_H__

#ifdef MAKEDLL
#define EXPORT_OBJECT __declspec(dllexport)
#else
#define EXPORT_OBJECT __declspec(dllimport)
#endif

#include <memory>
#include <functional>

class GenericRoot;

class EXPORT_OBJECT CCObject
{
public:
	using slotMethodType = std::function<void(CCObject*, std::shared_ptr<GenericRoot>)>;
	class CCObjectPimpl;	//Todo. 추후 private 으로 둘 방법을 찾아볼까

private:
	std::shared_ptr<CCObjectPimpl> m_impl;
	uint32_t m_objectId;

public:
	static uint32_t m_latestObjectId;

public:
	explicit CCObject();
	virtual ~CCObject();

private:
	uint32_t ObjectId() const { return m_objectId; };
	virtual void OnNewConnect(CCObject *receiver) { }
	uint32_t GetSignalInnerId(uint8_t signalId)
	{
		return (m_objectId << 8) | signalId;
	}

public:
	bool Connection(uint32_t signalId, CCObject *receiver, slotMethodType slotMethod);	//@brief. 상대가 나에게 연결된다
	void Disconnection(CCObject *receiver, uint32_t signalId = -1);	//@brief. 강제로 객체의 연결을 끊거나 연결된 객체가 소멸될 때 필요하다

//protected:
	void Emit(uint32_t signalId, std::shared_ptr<GenericRoot> messageData);				//@brief. 나에게 연결된 상대의 메서드 모두 호출
};

#endif