
#define MAKEDALL
#include "GenericData.h"
#include "CCObject.h"

class GenericData::GenericDataPimpl
{
public:
	GenericDataPimpl();
	~GenericDataPimpl();

	bool m_boolean;
	int m_integer;
	std::string m_str;
	double m_double;
	CCObject *m_object;
};

GenericData::GenericDataPimpl::GenericDataPimpl()
{ }

GenericData::GenericDataPimpl::~GenericDataPimpl()
{ }

GenericData::GenericData()
	: m_type(DataType::Empty), m_pimpl(std::make_unique<GenericDataPimpl>())
{ }

GenericData::GenericData(bool boolean)
	: m_type(DataType::Boolean), m_pimpl(std::make_unique<GenericDataPimpl>())
{
	m_pimpl->m_boolean = boolean;
}

GenericData::GenericData(int integer)
	: m_type(DataType::Integer), m_pimpl(std::make_unique<GenericDataPimpl>())
{
	m_pimpl->m_integer = integer;
}

GenericData::GenericData(const std::string &str)
	: m_type(DataType::String), m_pimpl(std::make_unique<GenericDataPimpl>())
{
	m_pimpl->m_str = str;
}

GenericData::GenericData(const char *cstr)
	: m_type(DataType::String), m_pimpl(std::make_unique<GenericDataPimpl>())
{
	m_pimpl->m_str = cstr;
}

GenericData::GenericData(double dValue)
	: m_type(DataType::Double), m_pimpl(std::make_unique<GenericDataPimpl>())
{
	m_pimpl->m_double = dValue;
}

GenericData::GenericData(CCObject *cobject)
	: m_type(DataType::Object), m_pimpl(std::make_unique<GenericDataPimpl>())
{
	m_pimpl->m_object = cobject;
}

GenericData::GenericData(const GenericData &cpy)
	: m_type(cpy.m_type), m_pimpl(std::make_unique<GenericDataPimpl>())
{
	switch (cpy.m_type)
	{
	case DataType::Boolean:
		m_pimpl->m_boolean = cpy.m_pimpl->m_boolean;
		break;
	case DataType::Double:
		m_pimpl->m_double = cpy.m_pimpl->m_double;
		break;
	case DataType::Integer:
		m_pimpl->m_integer = cpy.m_pimpl->m_integer;
		break;
	case DataType::Object:
		m_pimpl->m_object = cpy.m_pimpl->m_object;		//@brief. shallow copies
		break;
	case DataType::String:
		m_pimpl->m_str = cpy.m_pimpl->m_str;
	}
}

GenericData::~GenericData()
{ }

bool GenericData::ToBool(bool defaultValue) const
{
	return (m_type == DataType::Boolean) ? m_pimpl->m_boolean : defaultValue;
}

int GenericData::ToInt(int defaultValue) const
{
	return (m_type == DataType::Integer) ? m_pimpl->m_integer : defaultValue;
}

std::string GenericData::ToString(std::string defaultValue) const
{
	return (m_type == DataType::String) ? m_pimpl->m_str : defaultValue;
}

double GenericData::ToDouble(double defaultValue) const
{
	return (m_type == DataType::Double) ? m_pimpl->m_double : defaultValue;
}

CCObject* GenericData::ToObject(CCObject *defaultValue) const
{
	return (m_type == DataType::Object) ? m_pimpl->m_object : defaultValue;
}