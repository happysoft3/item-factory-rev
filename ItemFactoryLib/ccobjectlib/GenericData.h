
#ifndef GENERIC_DATA_H__
#define GENERIC_DATA_H__

#ifdef MAKEDLL
#define EXPORT_OBJECT __declspec(dllexport)
#else
#define EXPORT_OBJECT __declspec(dllimport)
#endif

#include <string>
#include <memory>

class CCObject;

class EXPORT_OBJECT GenericData
{
public:
	enum DataType
	{
		Empty,
		Boolean,
		Integer,
		String,
		Double,
		Object
	};

private:
	DataType m_type;

	class GenericDataPimpl;
	std::unique_ptr<GenericDataPimpl> m_pimpl;

public:
	explicit GenericData(void);
	explicit GenericData(bool boolean);
	explicit GenericData(int integer);
	explicit GenericData(const std::string &str);
	explicit GenericData(const char *cstr);
	explicit GenericData(double dValue);
	explicit GenericData(CCObject *cobject);
	GenericData(const GenericData &cpy);
	~GenericData();

	bool IsBool() const { return m_type == DataType::Boolean; }
	bool IsInteger() const { return m_type == DataType::Integer; }
	bool IsString() const { return m_type == DataType::String; }
	bool IsDouble() const { return m_type == DataType::Double; }
	bool IsObject() const { return m_type == DataType::Object; }

	bool ToBool(bool defaultValue = false) const;
	int ToInt(int defaultValue = 0) const;
	std::string ToString(std::string defaultValue = {}) const;
	double ToDouble(double defaultValue = 0.0) const;
	CCObject* ToObject(CCObject *defaultValue = nullptr) const;
};

#endif