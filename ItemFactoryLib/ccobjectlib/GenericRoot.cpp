#define MAKEDLL
#include "GenericRoot.h"
#include "CCObject.h"

#include "GenericData.h"

#include <algorithm>
#include <iterator>

class GenericRoot::GenericRootPimpl
{
public:
	GenericRootPimpl();
	~GenericRootPimpl();

	std::map<std::string, GenericData> rootData;
};

GenericRoot::GenericRootPimpl::GenericRootPimpl()
{ }

GenericRoot::GenericRootPimpl::~GenericRootPimpl()
{ }

GenericRoot::GenericRoot()
	: m_pimpl(std::make_unique<GenericRootPimpl>())
{ }

GenericRoot::GenericRoot(std::initializer_list<std::pair<std::string, GenericData>> argList)
	: m_pimpl(std::make_unique<GenericRootPimpl>())
{
	for (const std::pair<std::string, GenericData> &value : argList)
		m_pimpl->rootData.emplace(value.first, value.second);
}

GenericRoot::GenericRoot(const GenericRoot &cpy)
	: m_pimpl(std::make_unique<GenericRootPimpl>())
{
	m_pimpl->rootData = cpy.m_pimpl->rootData;
}

GenericRoot::~GenericRoot()
{ }

GenericRoot& GenericRoot::operator=(const GenericRoot &assign)
{
	m_pimpl->rootData = assign.m_pimpl->rootData;
	return *this;
}

uint32_t GenericRoot::Count() const
{
	return m_pimpl->rootData.size();
}

GenericRoot::iterator GenericRoot::begin()
{
	return m_pimpl->rootData.begin();
}

GenericRoot::iterator GenericRoot::end()
{
	return m_pimpl->rootData.end();
}

GenericRoot::const_iterator GenericRoot::begin() const
{
	return m_pimpl->rootData.cbegin();
}

GenericRoot::const_iterator GenericRoot::end() const
{
	return m_pimpl->rootData.cend();
}

GenericRoot::const_iterator GenericRoot::ConstFind(const std::string &key) const
{
	return m_pimpl->rootData.find(key);
}

GenericRoot::iterator GenericRoot::Find(const std::string &key)
{
	return m_pimpl->rootData.find(key);
}

void GenericRoot::Remove(const std::string &key)
{
	m_pimpl->rootData.erase(key);
}

std::list<std::string> GenericRoot::GetKeyList()
{
	std::list<std::string> keyList;

	std::transform(m_pimpl->rootData.begin(), m_pimpl->rootData.end(), std::insert_iterator<std::list<std::string>>(keyList, keyList.begin()),
		[](const std::pair<std::string, GenericData> &node)->std::string { return node.first; });
	return keyList;
}

std::list<GenericData> GenericRoot::ToList()
{
	std::list<GenericData> dataList;

	std::transform(m_pimpl->rootData.begin(), m_pimpl->rootData.end(), std::insert_iterator<std::list<GenericData>>(dataList, dataList.begin()),
		[](const std::pair<std::string, GenericData> &node)->GenericData { return node.second; });
	return dataList;
}

void GenericRoot::insert(const std::string &key, const GenericData &data)
{
	m_pimpl->rootData.emplace(key, data);
}

void GenericRoot::insert(const std::pair<std::string, GenericData> &data)
{
	m_pimpl->rootData.insert(data);
}

GenericRoot::iterator GenericRoot::insert(GenericRoot::const_iterator position, const GenericRoot::value_type &data)
{
	return m_pimpl->rootData.insert(position, data);
}

std::string GenericRoot::FirstKey() const
{
	return m_pimpl->rootData.begin()->first;
}

GenericData GenericRoot::FirstValue() const
{
	return m_pimpl->rootData.begin()->second;
}

GenericRoot::mapped_type GenericRoot::operator[](const std::string &key) const
{
	return m_pimpl->rootData[key];
}

GenericRoot::mapped_type& GenericRoot::operator[](const std::string &key)
{
	return m_pimpl->rootData[key];
}