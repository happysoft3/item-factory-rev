
#ifndef GENERIC_ROOT_H__
#define GENERIC_ROOT_H__

#ifdef MAKEDLL
#define EXPORT_OBJECT __declspec(dllexport)
#else
#define EXPORT_OBJECT __declspec(dllimport)
#endif

#include <map>
#include <memory>
#include <list>

class GenericData;

class EXPORT_OBJECT GenericRoot
{
public:
	using const_iterator = std::map<std::string, GenericData>::const_iterator;
	using iterator = std::map<std::string, GenericData>::iterator;
	using key_type = std::map<std::string, GenericData>::key_type;
	using mapped_type = std::map<std::string, GenericData>::mapped_type;
	using size_type = std::map<std::string, GenericData>::size_type;
	using value_type = std::map<std::string, GenericData>::value_type;

private:
	class GenericRootPimpl;
	std::unique_ptr<GenericRootPimpl> m_pimpl;

public:
	explicit GenericRoot();
	explicit GenericRoot(std::initializer_list<std::pair<std::string, GenericData>> argList);
	GenericRoot(const GenericRoot &cpy);
	~GenericRoot();

	GenericRoot& operator=(const GenericRoot &assign);


	uint32_t Count() const;
	iterator begin();
	iterator end();
	const_iterator begin() const;
	const_iterator end() const;

	const_iterator ConstFind(const std::string &key) const;
	iterator Find(const std::string &key);
	void Remove(const std::string &key);

	std::list<std::string> GetKeyList();
	std::list<GenericData> ToList();
	void insert(const std::string &key, const GenericData &data);
	void insert(const std::pair<std::string, GenericData> &data);
	iterator insert(const_iterator position, const value_type &data);

	std::string FirstKey() const;
	GenericData FirstValue() const;

	mapped_type operator[](const std::string &key) const;
	mapped_type& operator[](const std::string &key);
};

#endif
