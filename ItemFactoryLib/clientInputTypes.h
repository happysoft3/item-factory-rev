
#ifndef CLIENT_INPUT_TYPES_H__
#define CLIENT_INPUT_TYPES_H__

#include <stdint.h>


struct clientCreateParams
{
	uint16_t header;
	uint16_t thingId;
	uint16_t xProfile;
	uint16_t yProfile;
	uint8_t qualityId;
	uint8_t materialId;
	uint16_t magic1;
	uint16_t magic2;
	uint8_t createCount;
	uint8_t missileAmount;
	uint8_t someflags;
};



#endif