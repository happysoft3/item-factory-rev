
#include "clientProcThread.h"
#include "memPlugins.h"
#include "clientInputTypes.h"

#include "ccobjectlib\GenericRoot.h"
#include "ccobjectlib\GenericData.h"

ClientProcThread::ClientProcThread(std::shared_ptr<MemPlugins> plugins)
	: m_worker(), m_isWorking(false), m_forceStop(false)
{
	m_plugins = plugins;
	m_headerValue = 0;
	m_eraseHeader = 0x1;
}

ClientProcThread::~ClientProcThread()
{
	if (m_worker.joinable())
	{
		m_forceStop = true;
		m_worker.join();
	}
}

void ClientProcThread::Start()
{
	if (m_isWorking)
		return;

	m_isWorking = true;
	if (m_forceStop)
		m_forceStop = false;
	if (m_worker.joinable())
		m_worker.join();
	m_worker = std::thread([this]() { this->DoWork(); });
}

void ClientProcThread::Stop()
{
	if (m_isWorking)
		m_forceStop = true;
}

void ClientProcThread::Stopped()
{
	m_isWorking = false;
	Emit(workerSignals::STOPPED, std::make_shared<GenericRoot>());
}

void ClientProcThread::DoWork()
{
	while (!m_plugins.expired())
	{
		if (m_forceStop)
			break;
		std::shared_ptr<MemPlugins> plugins = m_plugins.lock();

		if (plugins->IsOpened())
		{
			CheckMessageAll();
			std::this_thread::sleep_for(std::chrono::milliseconds(200));
		}
		else
			break;
	}
	Stopped();
}

bool ClientProcThread::CaptureMessage(std::vector<uint8_t> &capture)
{
	clientCreateType *packet = reinterpret_cast<clientCreateType *>(capture.data());

	if (packet->header == m_headerValue)
	{
		uint16_t wordPad = 0x8000 - 1;
		uint8_t bytePad = 0x80 - 1;
		std::shared_ptr<GenericRoot> message(new GenericRoot);

		//Todo. 받은 패킷을 메시지에 담아서 Emit 한다
		//Emit된 메시지를 팩토리 객체가 듣고 아이템 생성 처리를 한다
		message->insert("#thingId", GenericData(static_cast<int>(packet->thingId & wordPad)));
		message->insert("#qualityId", GenericData(static_cast<int>(packet->qualityId & bytePad)));
		message->insert("#materialId", GenericData(static_cast<int>(packet->materialId & bytePad)));
		message->insert("#magic1Id", GenericData(static_cast<int>(packet->magic1 & wordPad)));
		message->insert("#magic2Id", GenericData(static_cast<int>(packet->magic2 & wordPad)));

		message->insert("x_position", GenericData(static_cast<int>(packet->xProfile & wordPad)));
		message->insert("y_position", GenericData(static_cast<int>(packet->yProfile & wordPad)));
		message->insert("createCount", GenericData(static_cast<int>(packet->createCount & 0xf)));

		bool invincible = (packet->someflags & 1) ? true : false;
		message->insert("invincible", GenericData(invincible));

		bool isVolatile = (packet->someflags & 2) ? true : false;
		message->insert("volatile", GenericData(isVolatile));

		message->insert("capacity", GenericData(static_cast<int>(packet->missileAmount)));
		message->insert("#netcode", GenericData(m_latestNetId & 0xffff));

		Emit(MSG_CAPTURE_COMPLETE, message);
		return true;
	}
	return false;
}

void ClientProcThread::CheckMessageAll()
{
	if (m_plugins.expired())
		return;
	std::shared_ptr<MemPlugins> plugins = m_plugins.lock();

	if (plugins->IsHostPlayer())
	{
		uint32_t readPosition = plugins->GetMemory<uint32_t>(0x6f8ad0);
		std::vector<uint8_t> capture(sizeof(clientCreateType));
		while (readPosition)
		{
			plugins->ReadStream<uint8_t>(readPosition, capture);
			m_latestNetId = plugins->GetMemory<uint16_t>(readPosition + 656);
			if (CaptureMessage(capture))
			{
				plugins->SetMemory<uint16_t>(readPosition, m_eraseHeader);
				break;
			}
			readPosition = plugins->GetMemory<uint32_t>(readPosition + 0x2b0);
		}
	}
}