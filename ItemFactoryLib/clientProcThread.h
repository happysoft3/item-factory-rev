
#ifndef CLIENT_PROC_THREAD_H__
#define CLIENT_PROC_THREAD_H__

#include "ccobjectlib\CCObject.h"

#include <thread>
#include <atomic>
#include <vector>

class MemPlugins;

class ClientProcThread : public CCObject
{
public:
	enum workerSignals
	{
		STOPPED = 512,
		MSG_CAPTURE_COMPLETE
	};

private:
	std::weak_ptr<MemPlugins> m_plugins;
	std::thread m_worker;
	std::atomic<bool> m_isWorking;
	std::atomic<bool> m_forceStop;
	uint16_t m_headerValue;
	uint16_t m_eraseHeader;

	uint16_t m_latestNetId;

public:
	explicit ClientProcThread(std::shared_ptr<MemPlugins> plugins);
	virtual ~ClientProcThread() override;

	void Start();
	void Stop();
	void SetMessageHeader(uint16_t header)
	{
		m_headerValue = header;
	}

private:
	void Stopped();
	void DoWork();

private:
	bool CaptureMessage(std::vector<uint8_t> &capture);
	void CheckMessageAll();
};

#endif