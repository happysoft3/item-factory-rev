
#define MAKEDLL
#include "factoryInterface.h"
#include "itemFactoryLib.h"

FactoryInterface::FactoryInterface()
{ }

FactoryInterface::~FactoryInterface()
{ }

std::unique_ptr<FactoryInterface> FactoryInterface::MakeInstance()
{
	return std::make_unique<ItemFactoryLib>();
}