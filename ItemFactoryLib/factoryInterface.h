
#ifndef FACTORY_INTERFACE_H__
#define FACTORY_INTERFACE_H__

#ifdef MAKEDLL
#define EXPORT_OBJECT __declspec(dllexport)
#else
#define EXPORT_OBJECT __declspec(dllimport)
#endif

#include <memory>

class CCObject;
//@brief. 추상 인터페이스
class EXPORT_OBJECT FactoryInterface
{
public:
	explicit FactoryInterface();
	virtual ~FactoryInterface();

	static std::unique_ptr<FactoryInterface> MakeInstance();
	virtual void ClientBinding(std::shared_ptr<CCObject> client) = 0;
};

#endif