
#define MAKEDLL

#include "itemFactoryLib.h"
#include "clientProcThread.h"
#include "abstractItem.h"
#include "weapons.h"
#include "armors.h"
#include "itemFx.h"
#include "ccobjectlib\GenericRoot.h"
#include "ccobjectlib\GenericData.h"
#include "memPlugins.h"
#include "clientInputTypes.h"

#include <algorithm>
ItemFactoryLib::ItemFactoryLib()
{
	m_plugins = std::make_shared<MemPlugins>();

	m_package["Weapons"] = std::make_unique<Weapons>(m_plugins);
	m_package["Armors"] = std::make_unique<Armors>(m_plugins);
	m_package["QualityProperty"] = std::make_unique<ItemFx>(m_plugins, propertyTypes::QUALITY);
	m_package["MaterialProperty"] = std::make_unique<ItemFx>(m_plugins, propertyTypes::MATERIAL);
	m_package["EnchantProperty"] = std::make_unique<ItemFx>(m_plugins, propertyTypes::ENCHANT);

	m_uiClient = nullptr;
}

ItemFactoryLib::~ItemFactoryLib()
{ }

void ItemFactoryLib::ClientBinding(std::shared_ptr<CCObject> client)
{ }

//@brief. 플러그인 객체 생성, 타겟 프로세스를 엽니다.
bool ItemFactoryLib::OpenTargetProcess(std::shared_ptr<GenericRoot>)
{
	if (m_plugins->IsOpened())
		return true;
	bool pResult = m_plugins->Open();

	MakePackage();
	std::shared_ptr<GenericRoot> message(new GenericRoot());
	message->insert("#result", GenericData(pResult));
	Emit(SignalDesc::REPORT_PROCESS_ALIVE, message);
	Emit(SignalDesc::THREAD_STANDBY, message);
	return pResult;
}

//@brief. 플러그인 객체로 부터 추상 아이템 노드를 구성합니다
//@brief. 이 메서드가 호출되면 노드 재구성을 위해 먼저 노드를 비워야 할것 같습니다
void ItemFactoryLib::MakePackage()
{
	if (m_plugins->IsOpened())
		std::for_each(m_package.begin(), m_package.end(), [](decltype(*m_package.begin()) &item) { item.second->DoInitialize(); });
}

void ItemFactoryLib::OnNewConnect(CCObject *receiver)
{ }

AbstractItem *ItemFactoryLib::FindPackage(const std::string &packageTypeName)
{
	auto keyIterator = m_package.find(packageTypeName);

	if (keyIterator != m_package.end())
		return keyIterator->second.get();
	return nullptr;
}

void ItemFactoryLib::SendErrorReport()
{
	MemPlugins::pluginsErrorType errorType = m_plugins->LastError();
	std::shared_ptr<GenericRoot> report = std::make_shared<GenericRoot>();
	std::string errorReportTitle("error_report");

	switch (errorType)
	{
	case MemPlugins::pluginsErrorType::REQUIRED_YOUR_HOSTING:
		report->insert(errorReportTitle, GenericData("이 기능은 당신이 호스팅 일 때에만 사용할 수 있습니다"));
		break;
	case MemPlugins::pluginsErrorType::WAIT_COOLDOWN:
		report->insert(errorReportTitle, GenericData("재사용 대기시간 입니다"));
		break;
	case MemPlugins::pluginsErrorType::DISCONNECT_TARGET:
		report->insert(errorReportTitle, GenericData("타겟 프로세스와 연결이 끊겼습니다"));
		break;
	case MemPlugins::pluginsErrorType::NOT_COOP_MODE:
		report->insert(errorReportTitle, GenericData("싱글 플레이 또는 퀘스트 게임에서만 사용가능합니다"));
		break;
	default:
		report->insert(errorReportTitle, GenericData("오류"));
	}
	Emit(SignalDesc::REPORT_ERROR, report);
}

void ItemFactoryLib::ReportUserPosition()
{
	std::vector<int> xypos(2);
	std::shared_ptr<GenericRoot> reportPos(new GenericRoot);

	m_plugins->GetPlayerPosition(xypos);
	reportPos->insert("x_position", GenericData(xypos[0]));
	reportPos->insert("y_position", GenericData(xypos[1]));

	Emit(SignalDesc::REPORT_USER_POSITION, reportPos);
}

uint32_t ItemFactoryLib::GetItemThingId(const std::string &key)
{
	AbstractItem *wItem = FindPackage("Weapons");
	AbstractItem *aItem = FindPackage("Armors");

	if (wItem != nullptr && aItem != nullptr)
	{
		uint32_t wSearch = wItem->GetThingID(key);
		uint32_t aSearch = aItem->GetThingID(key);

		return (wSearch != -1) ? wSearch : aSearch;
	}
	return 0;
}

std::string ItemFactoryLib::GetItemnameWithThingId(uint32_t thingId)
{
	AbstractItem *wItem = FindPackage("Weapons");
	AbstractItem *aItem = FindPackage("Armors");

	if (wItem != nullptr && aItem != nullptr)
	{
		std::string wsearch = wItem->FindKeyWithThingId(thingId);
		std::string asearch = aItem->FindKeyWithThingId(thingId);

		return (wsearch.length()) ? wsearch : ((asearch.length()) ? asearch : std::string());
	}
	return{};
}

void ItemFactoryLib::TargetConnectCompleted(std::shared_ptr<GenericRoot> result)
{
	bool isopen = (*result)["#result"].ToBool();

	if (isopen)
		m_clientThread->Start();
}

void ItemFactoryLib::ReceiveClientMessage(std::shared_ptr<GenericRoot> climsg)
{
	uint32_t thingId = (*climsg)["#thingId"].ToInt();
	std::string itemkey = GetItemnameWithThingId(thingId);

	if (itemkey.length())
	{
		climsg->insert("itemname", GenericData(itemkey));

		std::string qualkey = m_package["QualityProperty"]->FindKeyWithThingId((*climsg)["#qualityId"].ToInt());
		std::string materialkey = m_package["MaterialProperty"]->FindKeyWithThingId((*climsg)["#materialId"].ToInt());
		std::string magic1 = m_package["EnchantProperty"]->FindKeyWithThingId((*climsg)["#magic1Id"].ToInt());
		std::string magic2 = m_package["EnchantProperty"]->FindKeyWithThingId((*climsg)["#magic2Id"].ToInt());

		climsg->insert("quality", GenericData(qualkey));
		climsg->insert("material", GenericData(materialkey));
		climsg->insert("magic1", GenericData(magic1));
		climsg->insert("magic2", GenericData(magic2));
		climsg->insert("#client", GenericData(true));

		MakeNewItem(climsg);

		uint16_t netId = static_cast<uint16_t>((*climsg)["#netcode"].ToInt());

		m_plugins->NoxPrintConsole(10, m_plugins->GetPlayerNickname(m_plugins->GetPlayerIndexFromNetId(netId)) + " 님께서 아이템을 생성하였습니다");
	}
}

void ItemFactoryLib::TargetAliveReportAgain()
{
	std::shared_ptr<GenericRoot> report(new GenericRoot);

	report->insert("#result", GenericData(m_plugins->IsOpened()));
	Emit(SignalDesc::REPORT_PROCESS_ALIVE, report);
}

std::string ItemFactoryLib::GetItemsDescription(const std::string &data)
{
	AbstractItem *wItem = FindPackage("Weapons");
	AbstractItem *aItem = FindPackage("Armors");

	if (wItem != nullptr && aItem != nullptr)
	{
		std::string wResult = wItem->GetItemDesc(data);
		std::string aResult = aItem->GetItemDesc(data);

		return wResult.length() ? wResult : aResult;
	}
	return{};
}

void ItemFactoryLib::StartCommunicate(std::shared_ptr<GenericRoot> message)
{
	CCObject *client = message->FirstValue().ToObject();

	if (client == nullptr)
		return;
	m_uiClient = client;
	std::shared_ptr<GenericRoot> notify(new GenericRoot());

	notify->insert(std::string("complete"), GenericData(true));
	Emit(SignalDesc::START_COMMUNICATE, notify);

	client->Connection(SignalDesc::REQUEST_ITEM_LIST, this,
		[](CCObject *receiver, std::shared_ptr<GenericRoot> message) { dynamic_cast<ItemFactoryLib *>(receiver)->RequestExportItemList(message); });
	client->Connection(SignalDesc::REQUEST_CREATEITEM, this,
		[](CCObject *r, std::shared_ptr<GenericRoot> m) { dynamic_cast<ItemFactoryLib *>(r)->MakeNewItem(m); });
	client->Connection(SignalDesc::CORE_REQUEST_DESCIPT, this,
		[](CCObject *r, std::shared_ptr<GenericRoot> m) { dynamic_cast<ItemFactoryLib *>(r)->SendItemDescript(m); });
	client->Connection(SignalDesc::CORE_REQUEST_CREATEUNIT, this,
		[](CCObject *r, std::shared_ptr<GenericRoot> m) { dynamic_cast<ItemFactoryLib*>(r)->CreateNewObject(m); });
	client->Connection(CORE_REQUEST_CLIENTSEND, this,
		[](CCObject *r, std::shared_ptr<GenericRoot> m) { dynamic_cast<ItemFactoryLib*>(r)->ReceiveClientCreateRequest(m); });
	client->Connection(CORE_REQUEST_USER_POSITION, this,
		[](CCObject *r, std::shared_ptr<GenericRoot> m) { dynamic_cast<ItemFactoryLib*>(r)->ReportUserPosition(); });

	Connection(SignalDesc::THREAD_STANDBY, this, [this](CCObject *r, std::shared_ptr<GenericRoot> m) { this->TargetConnectCompleted(m); });

	m_clientThread = std::make_unique<ClientProcThread>(m_plugins);
	m_clientThread->SetMessageHeader(0xecaf);

	m_clientThread->Connection(ClientProcThread::STOPPED, this, [](CCObject *r, std::shared_ptr<GenericRoot> m) { dynamic_cast<ItemFactoryLib*>(r)->TargetAliveReportAgain(); });
	m_clientThread->Connection(ClientProcThread::MSG_CAPTURE_COMPLETE, this,
		[](CCObject *r, std::shared_ptr<GenericRoot> m) { dynamic_cast<ItemFactoryLib *>(r)->ReceiveClientMessage(m); });
}

void ItemFactoryLib::RequestExportItemList(std::shared_ptr<GenericRoot> message)	//리스트 요청(종류)
{
	std::string itemTypeName = message->FirstValue().ToString();
	auto findIterator = m_package.find(itemTypeName);
	if (findIterator != m_package.end())
	{
		std::shared_ptr<GenericRoot> listMessage(new GenericRoot);
		std::list<std::string> keylist = findIterator->second->GetKeyList();

		std::for_each(keylist.begin(), keylist.end(), [&listMessage](const std::string &key)
		{
			listMessage->insert(key, GenericData(key));
		});
		listMessage->insert("#itemTypeId", GenericData(findIterator->second->ItemTypeNumber()));
		Emit(SignalDesc::RELEASE_ITEM_LIST, listMessage);
	}
	/*else
		std::cout << "!RemoveMe! void ItemFactoryLib::RequestExportItemList(std::shared_ptr<GenericRoot> message) failed!" << itemTypeName << std::endl;*/
}

//@brief. 아이템 생성을 합니다
void ItemFactoryLib::MakeNewItem(std::shared_ptr<GenericRoot> message)
{
	if (!m_plugins->IsOpened())
		return;

	createItemType itemParams{};
	auto getPropertyAddr = [](AbstractItem *item, const std::string &findKey) ->uint32_t { return (item != nullptr) ? item->GetItemAddr(findKey) : 0; };

	itemParams.qualityAddr = getPropertyAddr(FindPackage("QualityProperty"), (*message)["quality"].ToString());
	itemParams.materialAddr = getPropertyAddr(FindPackage("MaterialProperty"), (*message)["material"].ToString());
	itemParams.firstmagicAddr = getPropertyAddr(FindPackage("EnchantProperty"), (*message)["magic1"].ToString());
	itemParams.secondmagicAddr = getPropertyAddr(FindPackage("EnchantProperty"), (*message)["magic2"].ToString());

	itemParams.itemName = (*message)["itemname"].ToString();
	itemParams.createCount = (*message)["createCount"].ToInt();
	itemParams.invincible = (*message)["invincible"].ToBool();
	itemParams.missileAmount = (*message)["capacity"].ToInt();
	itemParams.xProfile = (*message)["x_position"].ToInt();
	itemParams.yProfile = (*message)["y_position"].ToInt();
	itemParams.isVolatile = (*message)["volatile"].ToBool();

	bool clientRequest = (*message)["#client"].ToBool();

	itemParams.isClient = clientRequest;
	itemParams.createAtUserPos = clientRequest ? false : (*message)["atUserPos"].ToBool();

	if (!m_plugins->CreateItemWithParams(itemParams))
	{
		if (!clientRequest)
			SendErrorReport();
	}
	else if (!clientRequest)
	{
		Emit(SignalDesc::REPORT_CREATE_OK, std::make_shared<GenericRoot>());
		ReportUserPosition();
	}
}

void ItemFactoryLib::SendItemDescript(std::shared_ptr<GenericRoot> data)
{
	std::string key = (*data)["#key"].ToString();
	std::string value = (*data)["#value"].ToString();
	AbstractItem *package = FindPackage(key);
	std::string desc = (package != nullptr) ? package->GetItemDesc(value) : GetItemsDescription(value);

	data->insert("#desc", GenericData(desc));
	Emit(SEND_DESCRIPT_DATA, data);
}

void ItemFactoryLib::CreateNewObject(std::shared_ptr<GenericRoot> data)
{
	bool atUserPos = (*data)["#userpos"].ToBool();
	std::string unitname = (*data)["#data"].ToString();

	uint32_t posX = atUserPos ? 0 : (*data)["#xpos"].ToInt();
	uint32_t posY = atUserPos ? 0 : (*data)["#ypos"].ToInt();

	if (m_plugins->CreateObjectAt(unitname, posX, posY))
		ReportUserPosition();
	else
		SendErrorReport();
}

void ItemFactoryLib::ReceiveClientCreateRequest(std::shared_ptr<GenericRoot> request)
{
	if (m_plugins->IsOpened())
	{
		clientCreateType clientItem{};
		auto getPropertyId = [](AbstractItem *item, const std::string &key)->uint32_t { return (item != nullptr) ? item->GetThingID(key) : -1; };

		clientItem.thingId = GetItemThingId((*request)["itemname"].ToString());
		clientItem.qualityId = getPropertyId(FindPackage("QualityProperty"), (*request)["quality"].ToString());
		clientItem.materialId = getPropertyId(FindPackage("MaterialProperty"), (*request)["material"].ToString());
		clientItem.magic1 = getPropertyId(FindPackage("EnchantProperty"), (*request)["magic1"].ToString());
		clientItem.magic2 = getPropertyId(FindPackage("EnchantProperty"), (*request)["magic2"].ToString());

		clientItem.header = 0xecaf;
		clientItem.xProfile = (*request)["x_position"].ToInt() & 0x1fff;
		clientItem.yProfile = (*request)["y_position"].ToInt() & 0x1fff;
		clientItem.createCount = (*request)["createCount"].ToInt() & 0xff;

		uint8_t someflags = (*request)["invincible"].ToBool() & 1;
		someflags |= ((*request)["volatile"].ToBool() << 1);
		clientItem.someflags = someflags;
		clientItem.missileAmount = (*request)["capacity"].ToInt() & 0xff;
		m_plugins->ClientCreateItem(clientItem);
	}
}