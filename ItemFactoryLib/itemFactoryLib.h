
#ifndef ITEM_FACTORY_LIB_H__
#define ITEM_FACTORY_LIB_H__

#include "ccobjectlib\CCObject.h"
#include "factoryInterface.h"
#include <map>


class MemPlugins;
class AbstractItem;
class ClientProcThread;

class ItemFactoryLib : public CCObject, public FactoryInterface
{
public:
	enum SignalDesc
	{
		START_COMMUNICATE = 1,
		REPORT_PROCESS_ALIVE,
		RELEASE_ITEM_LIST,
		REQUEST_ITEM_LIST,
		REQUEST_CREATEITEM,
		REPORT_ERROR,
		REPORT_CREATE_OK,
		REPORT_USER_POSITION,
		CORE_REQUEST_DESCIPT,
		SEND_DESCRIPT_DATA,
		CORE_REQUEST_CREATEUNIT,
		CORE_REQUEST_CLIENTSEND,
		CORE_REQUEST_USER_POSITION,
		THREAD_STANDBY
	};

private:
	std::shared_ptr<MemPlugins> m_plugins;
	std::map<std::string, std::unique_ptr<AbstractItem>> m_package;
	std::unique_ptr<ClientProcThread> m_clientThread;
	CCObject *m_uiClient;

public:
	explicit ItemFactoryLib();
	virtual ~ItemFactoryLib() override;

	virtual void ClientBinding(std::shared_ptr<CCObject> client) override;
	EXPORT_OBJECT bool OpenTargetProcess(std::shared_ptr<GenericRoot>);

private:
	void MakePackage();
	virtual void OnNewConnect(CCObject *receiver) override;
	AbstractItem *FindPackage(const std::string &packageTypeName);

	void SendErrorReport();
	void ReportUserPosition();
	uint32_t GetItemThingId(const std::string &key);
	std::string GetItemnameWithThingId(uint32_t thingId);
	void TargetConnectCompleted(std::shared_ptr<GenericRoot> result);
	void ReceiveClientMessage(std::shared_ptr<GenericRoot> climsg);

	void TargetAliveReportAgain();
	std::string GetItemsDescription(const std::string &data);

public:	//@slots
	EXPORT_OBJECT void StartCommunicate(std::shared_ptr<GenericRoot> message);
	void RequestExportItemList(std::shared_ptr<GenericRoot> message);
	void MakeNewItem(std::shared_ptr<GenericRoot> message);
	void SendItemDescript(std::shared_ptr<GenericRoot> data);
	void CreateNewObject(std::shared_ptr<GenericRoot> data);
	void ReceiveClientCreateRequest(std::shared_ptr<GenericRoot> request);
};

#endif