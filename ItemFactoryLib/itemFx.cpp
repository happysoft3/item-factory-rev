
#include "itemFx.h"
#include "memPlugins.h"

ItemFx::ItemFx(std::shared_ptr<MemPlugins> plugins, propertyTypes fxType)
	: AbstractItem(plugins), m_fxType(fxType)
{ }

ItemFx::~ItemFx()
{ }

uint32_t ItemFx::SetBaseAddress()
{
	switch (m_fxType)
	{
	case propertyTypes::QUALITY:
		return 0x611c54;
	case propertyTypes::MATERIAL:
		return 0x611c58;
	case propertyTypes::ENCHANT:
		return 0x611C5C;
	default:
		break;
	}
	return 0;
}

uint32_t ItemFx::SetTableSize()
{
	return 136;
}

//@brief. 초기화 작업이 진행될 때, 메모리로 부터 데이터를 로드해 옵니다
bool ItemFx::DoInitialize(bool reset)
{
	//Todo. 초기화 작업이 필요하다면 여기에서
	return AbstractItem::DoInitialize(reset);
}

int ItemFx::ItemTypeNumber() const
{
	return static_cast<int>(TypeId::Type) + static_cast<int>(m_fxType);
}