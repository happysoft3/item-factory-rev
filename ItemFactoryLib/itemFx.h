
#ifndef ITEM_FX_H__
#define ITEM_FX_H__

#include "abstractItem.h"

//@brief. 아이템 속성
enum class propertyTypes
{
	QUALITY,
	MATERIAL,
	ENCHANT
};

class ItemFx : public AbstractItem
{
private:
	propertyTypes m_fxType;
	enum TypeId { Type = 3 };

public:
	explicit ItemFx(std::shared_ptr<MemPlugins> plugins, propertyTypes fxType = propertyTypes::QUALITY);
	virtual ~ItemFx() override;

private:
	virtual uint32_t SetBaseAddress() override;
	virtual uint32_t SetTableSize() override;

public:
	virtual bool DoInitialize(bool reset = false) override;
	virtual int ItemTypeNumber() const override;
};

#endif