
#include "memHandler.h"

//#include <iostream>		//DebugOnly

MemHandler::MemHandler(const CString &processName)
	: m_processName(processName), m_hWnd(nullptr)
{
	m_pid = 0;
	m_handle = nullptr;
	m_executorTimeout = 2000;
}

MemHandler::~MemHandler()
{ }

bool MemHandler::Open()
{
	m_hWnd = ::FindWindowEx(nullptr, nullptr, nullptr, m_processName);

	if (m_hWnd != nullptr)
	{
		::GetWindowThreadProcessId(m_hWnd, reinterpret_cast<LPDWORD>(&m_pid));
		m_handle.reset(WinHandle(::OpenProcess(MAXIMUM_ALLOWED, 0, m_pid)));

		if (m_handle != nullptr)
		{
			if (GetMemory<uint32_t>(validationCheck::PTR) == validationCheck::VALUE)
				return true;
			else
				m_handle.reset();
		}
	}
	return false;
}

bool MemHandler::IsOpened()
{
	return GetMemory<uint32_t>(validationCheck::PTR) == validationCheck::VALUE;
}

void MemHandler::BeforeExec(uint32_t codeAddr, uint32_t codeLength)
{ }

void MemHandler::ExternExec(const std::vector<uint8_t> &codes, const std::vector<uint8_t> &paramV)
{
	if (m_executor)
		return;		//@brief. executor is busy
	uint32_t dwSize = codes.size() + paramV.size();
	uint32_t *vStream = static_cast<uint32_t *>(::VirtualAllocEx(m_handle.get().operator HANDLE(), nullptr, dwSize, MEM_COMMIT, PAGE_EXECUTE_READWRITE));
	uint32_t allocAddr = reinterpret_cast<uint32_t>(vStream);

	//std::cout << "!RemoveMe! void MemHandler::ExternExec(const std::vector<uint8_t> &codes, const std::vector<uint8_t> &paramV)" << reinterpret_cast<uint32_t>(vStream) << std::endl;

	WriteStream<uint8_t>(allocAddr, codes);	//@brief. 코드영역을 씁니다
	WriteStream<uint8_t>(allocAddr + codes.size(), paramV);	//@brief. 파라메터 영역을 씁니다
	BeforeExec(allocAddr, codes.size());
	m_executor.reset(WinHandle(::CreateRemoteThread(m_handle.get().operator HANDLE(), nullptr, 0, reinterpret_cast<LPTHREAD_START_ROUTINE>(vStream), nullptr, CREATE_SUSPENDED, nullptr)));

	::ResumeThread(m_executor.get().operator HANDLE());
	::WaitForSingleObject(m_executor.get().operator HANDLE(), m_executorTimeout);

	VirtualFreeEx(m_handle.get().operator HANDLE(), reinterpret_cast<LPVOID>(vStream), 0, MEM_RELEASE);
	m_executor.reset();
}