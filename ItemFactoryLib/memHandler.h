
#ifndef MEM_HANDLER_H__
#define MEM_HANDLER_H__

#include "winHandle.h"

#include <atlstr.h>
#include <stdint.h>
#include <vector>
#include <memory>
#include <process.h>


class MemHandler
{
private:
	HWND m_hWnd;
	handlePtr m_handle;
	CString m_processName;
	UINT m_pid;

	uint32_t m_executorTimeout;

	handlePtr m_executor;

	enum validationCheck { PTR = 0x5BF870, VALUE = 0x49544341 };

public:
	explicit MemHandler(const CString &processName);
	virtual ~MemHandler();

	bool Open();
	bool IsOpened();
	
	template <class T>
	bool SetMemory(uint32_t addr, T value);

	template <class T>
	struct addr_return_type
	{
		using type = T;
	};

	template <class T>
	typename addr_return_type<T>::type GetMemory(uint32_t addr);

	template <class T>
	bool ReadStream(uint32_t addr, std::vector<T> &destV);

	template <class T>
	bool WriteStream(uint32_t addr, const std::vector<T> &srcV);

protected:
	virtual void BeforeExec(uint32_t codeAddr, uint32_t codeLength);
	void ExternExec(const std::vector<uint8_t> &codes, const std::vector<uint8_t> &paramV);
	inline void FixCallOpcode(uint32_t curAddr, uint32_t callFunction);
};

template <class T>
bool MemHandler::SetMemory(uint32_t addr, T value)
{
	return ::WriteProcessMemory(m_handle.get().operator HANDLE(), reinterpret_cast<LPVOID>(addr), &value, sizeof(T), nullptr) & 1;
}

template <class T>
typename MemHandler::addr_return_type<T>::type MemHandler::GetMemory(uint32_t addr)
{
	T readResult = 0;
	bool pResult = ::ReadProcessMemory(m_handle.get().operator HANDLE(), reinterpret_cast<LPCVOID>(addr), &readResult, sizeof(T), nullptr) & 1;

	return pResult ? readResult : 0;
}

template <class T>
bool MemHandler::ReadStream(uint32_t addr, std::vector<T> &destV)
{
	uint32_t rdLength = sizeof(T) * destV.size();

	return ::ReadProcessMemory(m_handle.get().operator HANDLE(), reinterpret_cast<LPCVOID>(addr), destV.data(), rdLength, nullptr) & 1;
}

template <class T>
bool MemHandler::WriteStream(uint32_t addr, const std::vector<T> &srcV)
{
	uint32_t wrLength = sizeof(T) * srcV.size();

	return ::WriteProcessMemory(m_handle.get().operator HANDLE(), reinterpret_cast<LPVOID>(addr), srcV.data(), wrLength, nullptr) & 1;
}

void MemHandler::FixCallOpcode(uint32_t curAddr, uint32_t callFunction)
{
	SetMemory<uint32_t>(curAddr + 1, callFunction - curAddr - 5);
}

#endif