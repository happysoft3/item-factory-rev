
#include "memPlugins.h"
#include "clientInputTypes.h"

#include "bytecode.h"
#include "utils\stringUtils.h"

#include <algorithm>
#include <iterator>

MemPlugins::MemPlugins()
	: MemHandler("Nox GUI")
{
	m_currentFps = 0;
	m_createCooldown = 60;
	m_lastError = pluginsErrorType::THIS_ISNOT_ERROR;
}

MemPlugins::~MemPlugins()
{ }

void MemPlugins::ClientCreateItem(const clientCreateType &params)
{
	if (!CheckClientCondition() || IsHostPlayer())
		return;

	std::vector<uint8_t> packet(sizeof(params));
	clientCreateParams *packetMask = reinterpret_cast<clientCreateParams *>(packet.data());
	uint16_t wordPad = 0x8000;
	uint8_t bytePad = 0x80;

	packetMask->header = params.header;
	packetMask->thingId = params.thingId | wordPad;
	packetMask->xProfile = params.xProfile | wordPad;
	packetMask->yProfile = params.yProfile | wordPad;
	packetMask->qualityId = params.qualityId | bytePad;
	packetMask->materialId = params.materialId | bytePad;
	packetMask->magic1 = params.magic1 | wordPad;
	packetMask->magic2 = params.magic2 | wordPad;
	packetMask->createCount = params.createCount | bytePad;
	packetMask->missileAmount = (params.missileAmount == 0) ? 20 : params.missileAmount;
	packetMask->someflags = params.someflags | bytePad;

	std::vector<uint8_t> send({ 0x73, 0x0, 0x61, 0x0, 0x79, 0x0, 0x20, 0 });
	std::transform(packet.begin(), packet.end(), std::insert_iterator<std::vector<uint8_t>>(send, send.end()),
		[](uint8_t byt) { return byt; });

	m_function = pluginsFunction::WRITE_CMD_LINE;
	ExternExec(BytecodeArea::noxCmdOutputV, send);
}

void MemPlugins::CmdOutput(const std::string &commandMessage)
{
	if (CheckClientCondition())
	{
		if (commandMessage.length() == 0)
		{
			m_lastError = pluginsErrorType::INVALID_PARAM;
			return;
		}
		std::wstring innerCmdMsg;

		StringUtils::AnsiToUnicode(commandMessage, innerCmdMsg);

		std::vector<uint8_t> innerParam;
		std::copy_n(reinterpret_cast<const uint8_t *>(innerCmdMsg.c_str()), innerCmdMsg.size() * 2, std::insert_iterator<std::vector<uint8_t>>(innerParam, innerParam.begin()));

		m_function = pluginsFunction::WRITE_CMD_LINE;

		ExternExec(BytecodeArea::noxCmdOutputV, innerParam);
	}
}

MemPlugins::pluginsErrorType MemPlugins::LastError() const
{
	return m_lastError;
}

bool MemPlugins::CreateItemWithParams(const createItemType &params)
{
	if (CheckServerCondition(params.isClient))
	{
		m_function = pluginsFunction::CREATE_INVENTORY;

		//Todo. 생성처리를 여기서!
		std::vector<uint8_t> innerParam(0x64);
		const std::string &itemname = params.itemName;
		std::transform(itemname.begin(), itemname.end(), innerParam.begin(), [](char byt)->uint8_t { return static_cast<uint8_t>(byt); });

		bool userPos = params.createAtUserPos;

		*reinterpret_cast<float *>(&innerParam[0x40]) = static_cast<float>(userPos ? GetUserX() : params.xProfile);
		*reinterpret_cast<float *>(&innerParam[0x44]) = static_cast<float>(userPos ? GetUserY() : params.yProfile);
		*reinterpret_cast<uint32_t *>(&innerParam[0x48]) = (params.createCount == 0) ? 1 : params.createCount;
		*reinterpret_cast<uint32_t *>(&innerParam[0x4c]) = params.missileAmount | (params.missileAmount << 8);
		*reinterpret_cast<uint32_t *>(&innerParam[0x50]) = params.qualityAddr;
		*reinterpret_cast<uint32_t *>(&innerParam[0x54]) = params.materialAddr;
		*reinterpret_cast<uint32_t *>(&innerParam[0x58]) = params.firstmagicAddr;
		*reinterpret_cast<uint32_t *>(&innerParam[0x5c]) = params.secondmagicAddr;
		innerParam[0x60] = (params.invincible ? 1 : 0) | (params.isVolatile ? 2 : 0);

		ExternExec(BytecodeArea::noxCreateInventoryV, innerParam);
		m_lastError = pluginsErrorType::THIS_ISNOT_ERROR;
		return true;
	}
	return false;
}

bool MemPlugins::CreateObjectAt(const std::string &unitname, uint32_t xProfile, uint32_t yProfile)
{
	if (CheckServerCondition())
	{
		m_function = pluginsFunction::CREATE_OBJECT;
		std::vector<uint8_t> innerParam(0x48);
		std::transform(unitname.begin(), unitname.end(), innerParam.begin(), [](char byt)->uint8_t { return static_cast<uint8_t>(byt); });

		if (!xProfile || !yProfile)
		{
			xProfile = GetUserX();
			yProfile = GetUserY();
		}
		*reinterpret_cast<float *>(&innerParam[0x40]) = static_cast<float>(xProfile);
		*reinterpret_cast<float *>(&innerParam[0x44]) = static_cast<float>(yProfile);

		ExternExec(BytecodeArea::noxCreateObjectV, innerParam);
		m_lastError = pluginsErrorType::THIS_ISNOT_ERROR;
		return true;
	}
	return false;
}

bool MemPlugins::SendChatMessage(uint8_t userId, uint32_t durate, const std::string &content)
{
	if (CheckClientCondition())
	{
		uint32_t playerPtr = GetMemory<uint32_t>(0x62f9e0 + (userId * 0x12dc));

		if (playerPtr && content.length())
		{
			std::wstring unicodeContent;
			std::vector<uint8_t> rawData((content.length() * 2) + 2, 0);

			StringUtils::AnsiToUnicode(content, unicodeContent);
			std::copy_n(reinterpret_cast<const uint8_t*>(unicodeContent.c_str()), content.length()*2, rawData.begin());
			
			std::vector<uint8_t> innerParam(8);

			*reinterpret_cast<uint32_t *>(&innerParam[0]) = durate;
			*reinterpret_cast<uint32_t *>(&innerParam[4]) = playerPtr;
			std::transform(rawData.begin(), rawData.end(), std::insert_iterator<std::vector<uint8_t>>(innerParam, innerParam.end()), [](uint8_t cpy) { return cpy; });

			m_function = pluginsFunction::SEND_CHATMESSAGE;
			ExternExec(BytecodeArea::noxChatMessageV, innerParam);

			return true;
		}
	}
	return false;
}

bool MemPlugins::NoxPrintConsole(uint8_t textColor, const std::string &content)
{
	if (CheckClientCondition())
	{
		std::wstring unicodeContent;
		std::vector<uint8_t> rawData((content.length() * 2) + 2, 0);

		StringUtils::AnsiToUnicode(content, unicodeContent);
		std::copy_n(reinterpret_cast<const uint8_t*>(unicodeContent.c_str()), content.length()*2, rawData.begin());

		std::vector<uint8_t> innerParam(4);
		innerParam[0] = textColor;
		std::transform(rawData.begin(), rawData.end(), std::insert_iterator<std::vector<uint8_t>>(innerParam, innerParam.end()), [](uint8_t cpy) { return cpy; });

		m_function = pluginsFunction::PRINT_CONSOLE;
		ExternExec(BytecodeArea::noxConsolePrint, innerParam);
		return true;
	}
	return false;
}

void MemPlugins::GetPlayerPosition(std::vector<int> &destV)
{
	if (destV.size() < 2)
		destV.resize(2);
	destV[0] = GetMemory<int>(0x69a5e8);
	destV[1] = GetMemory<int>(0x69a5ec);
}

bool MemPlugins::IsHostPlayer()
{
	return GetMemory<uint32_t>(0x654284) != 0;
}

uint8_t MemPlugins::GetPlayerIndexFromNetId(uint16_t netId)
{
	uint32_t playerNetcodeAddr = 0x62f9e4;

	for (int i = 0; i < 32; i++)
	{
		if (GetMemory<uint16_t>(playerNetcodeAddr) == netId)
			return static_cast<uint8_t>(i);
		playerNetcodeAddr += 0x12dc;
	}
	return -1;
}

std::string MemPlugins::GetPlayerNickname(uint8_t userId)
{
	if (userId == -1)
		return "null";
	std::vector<uint16_t> usernameV(18);

	ReadStream<uint16_t>(0x62fa61 + (userId * 0x12dc), usernameV);
	std::wstring name(usernameV.begin(), usernameV.end());
	std::string result;

	StringUtils::UnicodeToAnsi(name, result);
	return result.substr(0, result.find_first_of('\0'));
}

bool MemPlugins::IsIngame()
{
	return GetMemory<uint32_t>(0x853bb0) != 0;
}

uint32_t MemPlugins::GetFps()
{
	return GetMemory<uint32_t>(0x84ea04);
}

uint32_t MemPlugins::GetUserX()
{
	return GetMemory<uint32_t>(0x69a5e8);
}

uint32_t MemPlugins::GetUserY()
{
	return GetMemory<uint32_t>(0x69a5ec);
}

bool MemPlugins::IsCoopgameMode()
{
	return GetMemory<uint8_t>(0x85b7a0) == 0;
}

void MemPlugins::FixCreateInventory(uint32_t codeAddr, uint32_t argAddr)
{
	SetMemory<uint32_t>(codeAddr + 6, argAddr);
	FixCallOpcode(codeAddr + 0x1b, 0x4E3810);
	FixCallOpcode(codeAddr + 0x3d, 0x4DAA50);
	FixCallOpcode(codeAddr + 0xec, 0x4E79C0);
}

void MemPlugins::FixCmdLine(uint32_t codeAddr, uint32_t argAddr)
{
	SetMemory<uint32_t>(codeAddr + 3, argAddr);
	FixCallOpcode(codeAddr + 7, 0x443c80);
}

void MemPlugins::FixCreateObject(uint32_t codeAddr, uint32_t argAddr)
{
	SetMemory<uint32_t>(codeAddr + 5, argAddr);
	FixCallOpcode(codeAddr + 0xd, 0x4E3810);
	FixCallOpcode(codeAddr + 0x2e, 0x4daa50);
}

void MemPlugins::FixChatMessage(uint32_t codeAddr, uint32_t argAddr)
{
	SetMemory<uint32_t>(codeAddr + 6, argAddr);
	FixCallOpcode(codeAddr + 0x18, 0x528ac0);
}

void MemPlugins::FixPrintConsole(uint32_t codeAddr, uint32_t argAddr)
{
	SetMemory<uint32_t>(codeAddr + 5, argAddr);
	FixCallOpcode(codeAddr + 0x15, 0x450b90);
}

void MemPlugins::BeforeExec(uint32_t codeAddr, uint32_t codeLength)
{
	uint32_t argAddr = codeAddr + codeLength;
	MemHandler::BeforeExec(codeAddr, codeLength);

	switch (m_function)
	{
	case pluginsFunction::WRITE_CMD_LINE:
		FixCmdLine(codeAddr, argAddr);
		break;
	case pluginsFunction::SEND_CHATMESSAGE:
		FixChatMessage(codeAddr, argAddr);
		break;
	case pluginsFunction::PRINT_CONSOLE:
		FixPrintConsole(codeAddr, argAddr);
		break;
	case pluginsFunction::CREATE_INVENTORY:
		FixCreateInventory(codeAddr, argAddr);
		break;
	case pluginsFunction::CREATE_OBJECT:
	default:
		FixCreateObject(codeAddr, argAddr);
		break;
	}
}

bool MemPlugins::CheckCommonCondition()
{
	if (!IsOpened())
	{
		m_lastError = pluginsErrorType::DISCONNECT_TARGET;
		return false;
	}
	return true;
}

bool MemPlugins::CheckClientCondition()
{
	if (!CheckCommonCondition())
		return false;
	if (!IsIngame())
	{
		m_lastError = pluginsErrorType::NOT_INGAME;
		return false;
	}
	return true;
}

bool MemPlugins::CheckServerCondition(bool passIfClient)
{
	if (!CheckCommonCondition())
		return false;
	if (!IsHostPlayer())
	{
		m_lastError = pluginsErrorType::REQUIRED_YOUR_HOSTING;
		return false;
	}
	if (!IsCoopgameMode())
	{
		m_lastError = pluginsErrorType::NOT_COOP_MODE;
		return false;
	}
	if (passIfClient ? false : abs(static_cast<int>(GetFps() - m_currentFps)) <= m_createCooldown)
	{
		m_lastError = pluginsErrorType::WAIT_COOLDOWN;
		return false;
	}
	else if (!passIfClient)
		m_currentFps = GetFps();
	return true;
}