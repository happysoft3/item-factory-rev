
#ifndef MEM_PLUGINS_H__
#define MEM_PLUGINS_H__

#include "memHandler.h"

#include <memory>
#include <string>

struct clientCreateParams;
using clientCreateType = struct clientCreateParams;

using createItemType = struct
{
	std::string itemName;
	uint32_t qualityAddr;
	uint32_t materialAddr;
	uint32_t firstmagicAddr;
	uint32_t secondmagicAddr;

	uint32_t createCount;
	uint8_t missileAmount;
	bool invincible;
	bool createAtUserPos;
	bool isVolatile;
	uint32_t xProfile;
	uint32_t yProfile;
	bool isClient;
};

class MemPlugins : public MemHandler
{
public:
	enum pluginsErrorType
	{
		THIS_ISNOT_ERROR,
		REQUIRED_YOUR_HOSTING,
		WAIT_COOLDOWN,
		DISCONNECT_TARGET,
		INVALID_PARAM,
		NOT_COOP_MODE,
		NOT_INGAME
	};

	enum pluginsFunction
	{
		CREATE_INVENTORY,
		WRITE_CMD_LINE,
		CREATE_OBJECT,
		SEND_CHATMESSAGE,
		PRINT_CONSOLE
	};

private:
	uint32_t m_currentFps;
	int m_createCooldown;
	pluginsErrorType m_lastError;
	pluginsFunction m_function;

public:
	explicit MemPlugins();
	virtual ~MemPlugins() override;
	
	void ClientCreateItem(const clientCreateType &params);
	void CmdOutput(const std::string &commandMessage);
	pluginsErrorType LastError() const;
	bool CreateItemWithParams(const createItemType &params);
	bool CreateObjectAt(const std::string &unitname, uint32_t xProfile, uint32_t yProfile);
	bool SendChatMessage(uint8_t userId, uint32_t durate, const std::string &content);
	bool NoxPrintConsole(uint8_t textColor, const std::string &content);

	void GetPlayerPosition(std::vector<int> &destV);
	bool IsHostPlayer();

	uint8_t GetPlayerIndexFromNetId(uint16_t netId);
	std::string GetPlayerNickname(uint8_t userId);

private:
	bool IsIngame();
	uint32_t GetFps();
	uint32_t GetUserX();
	uint32_t GetUserY();
	bool IsCoopgameMode();
	void FixCreateInventory(uint32_t codeAddr, uint32_t argAddr);
	void FixCmdLine(uint32_t codeAddr, uint32_t argAddr);
	void FixCreateObject(uint32_t codeAddr, uint32_t argAddr);
	void FixChatMessage(uint32_t codeAddr, uint32_t argAddr);
	void FixPrintConsole(uint32_t codeAddr, uint32_t argAddr);
	virtual void BeforeExec(uint32_t codeAddr, uint32_t codeLength) override;

	bool CheckCommonCondition();
	bool CheckClientCondition();
	bool CheckServerCondition(bool passIfClient = false);
};

#endif