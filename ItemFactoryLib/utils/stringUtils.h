
#ifndef STRING_UTILS_H__
#define STRING_UTILS_H__

#include <string>

namespace StringUtils
{
	__declspec(dllexport) bool AnsiToUnicode(const std::string &src, std::wstring &dest);
	__declspec(dllexport) bool UnicodeToAnsi(std::wstring &src, std::string &dest);
	__declspec(dllexport) bool UnicodeToUtf8(std::wstring &src, std::string &dest);
	__declspec(dllexport) bool Utf8ToUnicode(std::string &src, std::wstring &dest);
	__declspec(dllexport) std::string GetFileExtension(const std::string &fullName);
}

#endif
