
#include "weapons.h"
#include "memPlugins.h"

Weapons::Weapons(std::shared_ptr<MemPlugins> plugins)
	: AbstractItem(plugins)
{ }

Weapons::~Weapons()
{ }

uint32_t Weapons::SetBaseAddress()
{
	return 0x611C64;
}

uint32_t Weapons::SetTableSize()
{
	return 80;
}

//@brief. 초기화 작업이 진행될 때, 메모리로 부터 데이터를 로드해 옵니다
bool Weapons::DoInitialize(bool reset)
{
	//Todo. 초기화 작업이 필요하다면 여기에서
	return AbstractItem::DoInitialize(reset);
}

int Weapons::ItemTypeNumber() const
{
	return TypeId::Type;
}