
#ifndef WEAPONS_H__
#define WEAPONS_H__

#include "abstractItem.h"

//@brief. ����
class Weapons : public AbstractItem
{
private:
	enum TypeId { Type = 2 };

public:
	explicit Weapons(std::shared_ptr<MemPlugins> plugins);
	virtual ~Weapons() override;

private:
	virtual uint32_t SetBaseAddress() override;
	virtual uint32_t SetTableSize() override;

public:
	virtual bool DoInitialize(bool reset = false) override;
	virtual int ItemTypeNumber() const override;
};

#endif