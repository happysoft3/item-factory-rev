
#ifndef WIN_HANDLE_H__
#define WIN_HANDLE_H__

#include <windows.h>
#include <memory>

class WinHandle
{
private:
	HANDLE m_handleReal;

public:
	explicit WinHandle(std::nullptr_t = nullptr) : m_handleReal(nullptr) { }
	explicit WinHandle(HANDLE value) : m_handleReal(value == INVALID_HANDLE_VALUE ? nullptr : value) { }
	~WinHandle();

	explicit operator bool() const
	{
		return m_handleReal != nullptr;
	}

	operator HANDLE() const
	{
		return m_handleReal;
	}

	friend bool operator ==(WinHandle l, WinHandle r)
	{
		return l.m_handleReal == r.m_handleReal;
	}

	friend bool operator !=(WinHandle l, WinHandle r)
	{
		return !(l == r);
	}

	struct Deleter
	{
		using pointer = WinHandle;

		void operator()(WinHandle handle) const
		{
			CloseHandle(handle);
		}
	};
};

inline bool operator ==(HANDLE l, WinHandle r)
{
	return WinHandle(l) == r;
}

inline bool operator !=(HANDLE l, WinHandle r)
{
	return !(l == r);
}

inline bool operator ==(WinHandle l, HANDLE r)
{
	return l == WinHandle(r);
}

inline bool operator !=(WinHandle l, HANDLE r)
{
	return !(l == r);
}

using handlePtr = std::unique_ptr<WinHandle, WinHandle::Deleter>;

#endif