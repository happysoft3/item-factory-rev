
// ItemFactoryRevDlg.cpp : 구현 파일
//

#include "stdafx.h"
#include "ItemFactoryRev.h"
#include "ItemFactoryRevDlg.h"
#include "PageManager.h"
#include "coreUi.h"

#include "mainPage.h"
#include "subPage.h"
#include "settingPage.h"

#include "afxdialogex.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

#include <algorithm>


// 응용 프로그램 정보에 사용되는 CAboutDlg 대화 상자입니다.

class CAboutDlg : public CDialogEx
{
public:
	CAboutDlg();

// 대화 상자 데이터입니다.
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_ABOUTBOX };
#endif

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

// 구현입니다.
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialogEx(IDD_ABOUTBOX)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialogEx)
END_MESSAGE_MAP()


// CItemFactoryRevDlg 대화 상자



CItemFactoryRevDlg::CItemFactoryRevDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(IDD_ITEMFACTORYREV_DIALOG, pParent), m_bkBrush(new CBrush(RGB(84, 80, 158)))
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
	m_core = CoreUi::MakeInstance();
	m_core->RegistMainWndEventCallback(this, &CItemFactoryRevDlg::EventMessageCallback);
	m_core->Initialize();
	m_pageLoader = nullptr;
}

void CItemFactoryRevDlg::InitCControls()
{
	CWnd *loaderFrame = this->GetDlgItem(MAINWND_LOADER);
	m_pages = PageManager::MakeInstance(*loaderFrame, this);

	m_pages->MakePage("mainPage", std::move(std::unique_ptr<CWnd>(new MainPage(IDD_FORMVIEW))) );
	m_pages->MakePage("settingPage", std::move(std::unique_ptr<CWnd>(new SettingPage(IDD_FORMVIEW1))) );
	m_pages->MakePage("creatorPage", std::move(std::unique_ptr<CWnd>(new SubPage(IDD_FORMVIEW2))) );

	m_pages->ShowPage("mainPage");

	m_goFirstPageBtn.ChangeButtonCaption(L"인벤토리 생성");
	m_goFirstPageBtn.SetState(TRUE);
	m_goSecondPageBtn.ChangeButtonCaption(L"아이템 생성");
	m_goThirdPageBtn.ChangeButtonCaption(L"옵션");
	m_pageButtons.emplace("mainPage", &m_goFirstPageBtn);
	m_pageButtons.emplace("creatorPage", &m_goSecondPageBtn);
	m_pageButtons.emplace("settingPage", &m_goThirdPageBtn);

	m_statText.SetWindowTextW(L"연결 대기중");
	m_statText.SetTextColor(RGB(90, 95, 165));

	m_core->RegistPageManager(m_pages);

	m_pageLoader = nullptr;

	SetWindowTextW(L"아이템 팩토리__ revision - made by 'panic'. release: may 5 2021");
}

void CItemFactoryRevDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);

	DDX_Control(pDX, MAINWND_BUTTON1, m_goFirstPageBtn);
	DDX_Control(pDX, MAINWND_BUTTON2, m_goSecondPageBtn);
	DDX_Control(pDX, MAINWND_BUTTON3, m_goThirdPageBtn);

	DDX_Control(pDX, MAINWND_STAT_TEXT, m_statText);

	InitCControls();
}

void CItemFactoryRevDlg::UpdateStateLabel(bool state)
{
	if (state)
	{
		m_statText.SetTextColor(RGB(44, 143, 53));
		m_statText.SetWindowTextW(L"녹스 ON");
	}
	else
	{
		m_statText.SetTextColor(RGB(227, 28, 78));
		m_statText.SetWindowTextW(L"녹스 OFF");
	}
}

void CItemFactoryRevDlg::SetCoreRequirements()
{
	//m_core->RegistPageManager(m_pages);
}

void CItemFactoryRevDlg::ReceiveMessage(uint32_t messageId, const std::string &message)
{
	switch (messageId)
	{
	case CoreUi::UiEvent::NOTIFY_STATE:
		UpdateStateLabel(message.compare("ok") == 0 ? true : false);
		break;
	case CoreUi::UiEvent::CONNECTION_OK:
		SetCoreRequirements();
		break;
	}
}

void CItemFactoryRevDlg::ChangeScreen(const std::string &pagename)
{
	m_pages->ShowPage(pagename);
	std::for_each(m_pageButtons.begin(), m_pageButtons.end(), [pagename](const auto &item)
	{
		item.second->SetState((pagename.compare(item.first) == 0) ? TRUE : FALSE);
	});
}

void CItemFactoryRevDlg::EventMessageCallback(CWnd *wnd, uint32_t messageId, std::string message)
{
	if (wnd != nullptr)
	{
		CItemFactoryRevDlg *cWnd = dynamic_cast<CItemFactoryRevDlg *>(wnd);

		cWnd->ReceiveMessage(messageId, message);
	}
}

BEGIN_MESSAGE_MAP(CItemFactoryRevDlg, CDialogEx)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_CLOSE()
	ON_BN_CLICKED(MAINWND_BUTTON1, OnGoFirstPageClick)
	ON_BN_CLICKED(MAINWND_BUTTON2, OnGoSecondPageClick)
	ON_BN_CLICKED(MAINWND_BUTTON3, OnGoThirdPageClick)
	ON_WM_QUERYDRAGICON()
	ON_WM_CTLCOLOR()
END_MESSAGE_MAP()


// CItemFactoryRevDlg 메시지 처리기

BOOL CItemFactoryRevDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// 시스템 메뉴에 "정보..." 메뉴 항목을 추가합니다.

	// IDM_ABOUTBOX는 시스템 명령 범위에 있어야 합니다.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		BOOL bNameValid;
		CString strAboutMenu;
		bNameValid = strAboutMenu.LoadString(IDS_ABOUTBOX);
		ASSERT(bNameValid);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// 이 대화 상자의 아이콘을 설정합니다.  응용 프로그램의 주 창이 대화 상자가 아닐 경우에는
	//  프레임워크가 이 작업을 자동으로 수행합니다.
	SetIcon(m_hIcon, TRUE);			// 큰 아이콘을 설정합니다.
	SetIcon(m_hIcon, FALSE);		// 작은 아이콘을 설정합니다.

	// TODO: 여기에 추가 초기화 작업을 추가합니다.

	return TRUE;  // 포커스를 컨트롤에 설정하지 않으면 TRUE를 반환합니다.
}

void CItemFactoryRevDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialogEx::OnSysCommand(nID, lParam);
	}
}

// 대화 상자에 최소화 단추를 추가할 경우 아이콘을 그리려면
//  아래 코드가 필요합니다.  문서/뷰 모델을 사용하는 MFC 응용 프로그램의 경우에는
//  프레임워크에서 이 작업을 자동으로 수행합니다.

void CItemFactoryRevDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // 그리기를 위한 디바이스 컨텍스트입니다.

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// 클라이언트 사각형에서 아이콘을 가운데에 맞춥니다.
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// 아이콘을 그립니다.
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

void CItemFactoryRevDlg::OnClose()
{
	std::for_each(m_pageButtons.begin(), m_pageButtons.end(), [](const auto &item) { item.second->SetState(FALSE); });
	if (m_pages)
		m_pages.reset();
	CDialogEx::OnClose();
}

void CItemFactoryRevDlg::OnGoFirstPageClick()
{
	ChangeScreen("mainPage");
	m_statText.SetFocus();
}

void CItemFactoryRevDlg::OnGoSecondPageClick()
{
	ChangeScreen("creatorPage");
	m_statText.SetFocus();
}

void CItemFactoryRevDlg::OnGoThirdPageClick()
{
	ChangeScreen("settingPage");
	m_statText.SetFocus();
}


// 사용자가 최소화된 창을 끄는 동안에 커서가 표시되도록 시스템에서
//  이 함수를 호출합니다.
HCURSOR CItemFactoryRevDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

HBRUSH CItemFactoryRevDlg::OnCtlColor(CDC *pDC, CWnd *pWnd, UINT nCtlColor)
{
	CDialogEx::OnCtlColor(pDC, pWnd, nCtlColor);

	//pDC->SetBkColor(m_bkColor);
	return *m_bkBrush;

}

BOOL CItemFactoryRevDlg::PreTranslateMessage(MSG* pMsg)
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.
	if (pMsg->message == WM_KEYDOWN)
	{
		//if (pMsg->wParam == VK_RETURN) // ENTER키 눌릴 시
		//	return TRUE;
		//if (pMsg->wParam == VK_ESCAPE) // ESC키 눌릴 시
		//	return TRUE;
		switch (pMsg->wParam)
		{
		case VK_RETURN:
		case VK_ESCAPE:
		case VK_F1:
			return TRUE;
		}
	}
	return CDialogEx::PreTranslateMessage(pMsg);
}
