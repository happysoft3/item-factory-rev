
// ItemFactoryRevDlg.h : 헤더 파일
//

#pragma once

#include "UI\CColorBtn.h"
#include "UI\TextLabel.h"

#include <memory>
#include <string>
#include <map>

class PageManager;
class CoreUi;

// CItemFactoryRevDlg 대화 상자
class CItemFactoryRevDlg : public CDialogEx
{
private:
	CColorBtn m_goFirstPageBtn;
	CColorBtn m_goSecondPageBtn;
	CColorBtn m_goThirdPageBtn;
	TextLabel m_statText;
	std::shared_ptr<PageManager> m_pages;
	std::shared_ptr<CoreUi> m_core;
	std::map<std::string, CButton*> m_pageButtons;

	std::unique_ptr<CBrush> m_bkBrush;

	CWnd *m_pageLoader;

// 생성입니다.
public:
	CItemFactoryRevDlg(CWnd* pParent = NULL);	// 표준 생성자입니다.

private:
	void InitCControls();

// 대화 상자 데이터입니다.
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_ITEMFACTORYREV_DIALOG };
#endif


protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV 지원입니다.

private:
	void UpdateStateLabel(bool state);
	void SetCoreRequirements();
	void ReceiveMessage(uint32_t messageId, const std::string &messge);
	void ChangeScreen(const std::string &pagename);

public:
	static void EventMessageCallback(CWnd *wnd, uint32_t messageId, std::string message);

// 구현입니다.
protected:
	HICON m_hIcon;

	// 생성된 메시지 맵 함수
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg void OnClose();
	afx_msg void OnGoFirstPageClick();
	afx_msg void OnGoSecondPageClick();
	afx_msg void OnGoThirdPageClick();
	afx_msg HCURSOR OnQueryDragIcon();
	afx_msg HBRUSH OnCtlColor(CDC *pDC, CWnd *pWnd, UINT nCtlColor);
	afx_msg BOOL PreTranslateMessage(MSG* pMsg);
	DECLARE_MESSAGE_MAP()
};
