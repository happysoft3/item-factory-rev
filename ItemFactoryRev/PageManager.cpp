
#include "stdafx.h"
#include "PageManager.h"
#include "interactPage.h"
#include "ccobjectlib\GenericRoot.h"
#include "ccobjectlib\GenericData.h"
#include "pageUiSignalsDef.h"
#include "corePageManagerSignalsDef.h"
#include <algorithm>

struct ConcretePageManager : public PageManager
{
	explicit ConcretePageManager(const CWnd &targetWnd, CWnd *parent)
		: PageManager(targetWnd, parent)
	{ }
};

PageManager::PageManager(const CWnd &targetWnd, CWnd *parent)
{
	m_parent = parent;
	::ZeroMemory(&m_context, sizeof(m_context));
	targetWnd.GetWindowRect(&m_screenArea);

	//@brief. LoaderFrame Alignment
	m_screenArea.top -= 30;
	m_screenArea.left -= 8;
	m_screenArea.bottom -= 30;
	m_screenArea.right -= 8;

	m_pageCb = nullptr;

	m_syncValue = 0;
}

//@brief. 소멸자
PageManager::~PageManager()
{ }

//@brief. 인스턴스 생성
std::shared_ptr<PageManager> PageManager::MakeInstance(const CWnd &target, CWnd *parent)
{
	std::shared_ptr<PageManager> self = std::make_shared<ConcretePageManager>(target, parent);

	self->SetSelf(self);
	return self;
}

void PageManager::DoAction(std::shared_ptr<GenericRoot> m)
{
	/*auto keyIterator = m->Find("#action");

	if (keyIterator == m->end())
		return;
	std::string action = keyIterator->second.ToString();

	if (!action.compare("init"))
	{
		Connection(CoreManagerSignals::REQUEST_ITEM_CREATE, this,
			[](CCObject *r, std::shared_ptr<GenericRoot> m)
		{
			std::string pagename = (*m)["#pagename"].ToString();
			uint32_t notify = (*m)["#notify"].ToInt();

			dynamic_cast<PageManager *>(r)->SendMessageToScreen(pagename, notify, m);
		});
	}*/
}

//@brief. 페이지 생성
bool PageManager::MakePage(const std::string &pageKey, std::unique_ptr<CWnd> wndObject)
{
	std::map<std::string, std::unique_ptr<CWnd>>::iterator objectIterator = m_objectMap.find(pageKey);

	if (objectIterator != m_objectMap.end())
	{
		//@brief. already exist!
		return false;
	}
	InteractPage *tabPage = dynamic_cast<InteractPage *>(wndObject.get());

	if (tabPage->Create(nullptr, nullptr, WS_CHILD | WS_VSCROLL | WS_HSCROLL, m_screenArea, m_parent, &m_context))
	{
		tabPage->OnInitialUpdate();
		tabPage->SetPagename(pageKey);
		if (!m_self.expired())
			tabPage->SetManager(m_self.lock(), &PageManager::Subscriber);
		m_objectMap.emplace(pageKey, std::move(wndObject));
		return true;
	}
	return false;
}

//@brief. 특정 페이지를 보여줍니다
bool PageManager::ShowPage(const std::string &pageKey)
{
	std::map<std::string, std::unique_ptr<CWnd>>::iterator objectIterator = m_objectMap.find(pageKey);

	while (objectIterator != m_objectMap.end())
	{
		if (!m_currentScreenName.compare(pageKey))
			return true;

		CTabPage *currentPage = dynamic_cast<CTabPage *>(objectIterator->second.get());
		if (currentPage == nullptr)
			break;

		currentPage->Show();
		while (m_currentScreenName.length())
		{
			CTabPage *previousPage = dynamic_cast<CTabPage *>(m_objectMap[m_currentScreenName].get());
			if (previousPage == nullptr)
				break;
			previousPage->Show(false);
			break;
		}
		m_currentScreenName = pageKey;
		return true;
	}
	return false;
}

//@brief. 생성된 탭을 모두 지웁니다	//아직까지 안쓰이는 메서드입니다
void PageManager::DestroyAll()
{
	std::for_each(m_objectMap.begin(), m_objectMap.end(), [](decltype(*m_objectMap.begin()) item)
	{
		item.second->DestroyWindow();
		item.second.reset();
	});
}

void PageManager::SetSelf(std::shared_ptr<CCObject> self)
{
	m_self = self;
}

void PageManager::MessageProcess(uint32_t actionId, const std::string &message)
{
	std::shared_ptr<GenericRoot> sendData = std::make_shared<GenericRoot>();

	switch (actionId)
	{
	case PageSignals::SETTING_CONNECT:
		Emit(CoreManagerSignals::TRY_CONNECTION, sendData);
		break;

	case PageSignals::MAINPAGE_UPDATELIST:
		//리스트 업데이트 요청.
		sendData->insert("listupdate", GenericData(static_cast<int>(m_syncValue)));
		Emit(CoreManagerSignals::REQUEST_LIST_UPDATE, sendData);
		break;

	case PageSignals::SETTINGPAGE_UPDATE_REQUEST:
		//세팅 페이지 업데이트 요청
		Emit(CoreManagerSignals::REQUEST_SETTING_PARAMS, sendData);
		break;

	case PageSignals::SETTINGPAGE_COUNT_AYSNC_REQUEST:
		sendData->insert("createCount", GenericData(message));
		Emit(CoreManagerSignals::ASYNC_COUNT_VALUE, sendData);
		break;

	case PageSignals::MAINPAGE_CLIENT_SEND:
		break;
	}

}

void PageManager::Subscriber(CCObject *obj, uint32_t actionId, std::string message)
{
	if (obj != nullptr)
	{
		PageManager *manager = dynamic_cast<PageManager *>(obj);

		manager->MessageProcess(actionId, message);
	}
}

CWnd* PageManager::FindScreen(const std::string &screenName)
{
	auto screenFind = m_objectMap.find(screenName);

	if (screenFind != m_objectMap.end())
	{
		return screenFind->second.get();
	}
	return nullptr;
}

void PageManager::SetSyncValue(std::shared_ptr<GenericRoot> message)
{
	uint32_t syncValue = (*message)["syncUpdate"].ToInt();
	std::string pagename = (*message)["#pagename"].ToString();

	m_syncValue = syncValue;
	SendMessageToScreen(pagename, PageSignals::MAINPAGE_CLEAR_ALL_LIST, std::shared_ptr<GenericRoot>());
}

void PageManager::UpdateSettingParams(std::shared_ptr<GenericRoot> message)
{
	std::string pagename = (*message)["#pagename"].ToString();

	SendMessageToScreen(pagename, PageSignals::SETTINGPAGE_UPDATE_PARAM, message);
}

void PageManager::UpdateItemDescription(std::shared_ptr<GenericRoot> data)
{
	std::string pagename = (*data)["#pagename"].ToString();

	SendMessageToScreen(pagename, PageSignals::MAINPAGE_UPDATE_DESCRIPT, data);
}

void PageManager::SendMessageToScreen(const std::string &screenName, uint32_t notifyType, std::shared_ptr<GenericRoot> message)
{
	InteractPage *findPage = dynamic_cast<InteractPage *>(FindScreen(screenName));

	if (findPage != nullptr)
		findPage->ScreenMessage(notifyType, message);
}