
#ifndef PAGE_MANAGER_H__
#define PAGE_MANAGER_H__

#include "ccobjectlib\CCObject.h"

#include <string>
#include <memory>
#include <map>

struct ConcretePageManager;

class PageManager : public CCObject
{
private:
	CCreateContext m_context;
	CRect m_screenArea;
	CWnd *m_parent;
	std::map<std::string, std::unique_ptr<CWnd>> m_objectMap;
	std::string m_currentScreenName;

	std::function<void()> m_pageCb;
	std::weak_ptr<CCObject> m_self;

	uint32_t m_syncValue;

private:
	explicit PageManager(const CWnd &target, CWnd *parent);

public:
	PageManager(const PageManager &) = delete;
	PageManager(PageManager &&) = delete;
	virtual ~PageManager() override;

	PageManager& operator=(const PageManager &) = delete;

	static std::shared_ptr<PageManager> MakeInstance(const CWnd &target, CWnd *parent);

	void DoAction(std::shared_ptr<GenericRoot> msg);
	bool MakePage(const std::string &pageKey, std::unique_ptr<CWnd> wndObject);
	bool ShowPage(const std::string &pageKey);

	friend struct ConcretePageManager;

private:
	void DestroyAll();
	void SetSelf(std::shared_ptr<CCObject> self);
	void MessageProcess(uint32_t actionId, const std::string &message);
	static void Subscriber(CCObject *obj, uint32_t actionId, std::string message);

	CWnd* FindScreen(const std::string &screenName);

public:
	void SetSyncValue(std::shared_ptr<GenericRoot> message);
	void UpdateSettingParams(std::shared_ptr<GenericRoot> message);
	void UpdateItemDescription(std::shared_ptr<GenericRoot> data);
	void SendMessageToScreen(const std::string &screenName, uint32_t notifyType, std::shared_ptr<GenericRoot> message);
};

#endif