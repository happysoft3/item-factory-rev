
#ifndef C_TAB_PAGE_H__
#define C_TAB_PAGE_H__

#include <stdint.h>

class CTabPage : public CFormView
{
	DECLARE_DYNAMIC(CTabPage)
private:
	UINT m_nIDTemplate;
	CBrush m_bkBrush;
	UINT m_bkColor;

public:
	CTabPage(UINT _nIDTemplate);
	virtual ~CTabPage();

	void ChangeBkColor(UINT _color);
	virtual BOOL Create(LPCTSTR lpszClassName, LPCTSTR lpszWindowName, DWORD dwStyle, const RECT& rect, CWnd* pParentWnd, CCreateContext *pContext = nullptr);
	virtual void OnInitialUpdate();
	virtual void OnEnterScreen();
	virtual void OnExitScreen();
	void Show(bool show = true);

protected:
	virtual void DoDataExchange(CDataExchange *pDX);
	afx_msg HBRUSH OnCtlColor(CDC *pDC, CWnd *pWnd, UINT nCtlColor);
	//afx_msg 
	DECLARE_MESSAGE_MAP()
};

#endif