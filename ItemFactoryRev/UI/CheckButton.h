
#ifndef CHECK_BUTTON_H__
#define CHECK_BUTTON_H__

#include <functional>

class CheckButton : public CMFCButton
{
	DECLARE_DYNAMIC(CheckButton)
private:
	bool m_status;
	UINT m_checkedColor;
	UINT m_uncheckedColor;
	UINT m_checkedBkColor;
	UINT m_uncheckedBkColor;
	CString m_checkText;
	CString m_uncheckText;

	uint32_t m_clickdata;
	CWnd *m_receiver;
	using clickCallbackType = std::function<void(CWnd*, uint32_t, bool)>;
	clickCallbackType m_onClicked;

public:
	explicit CheckButton();
	virtual ~CheckButton() override;

	bool GetCheckStatus() const { return m_status; };
	void SetCheckStatus(bool state);
	void SetConditionalBkColor(UINT checkedColor, UINT uncheckColor);
	void SetConditionalTextColor(UINT checkedColor, UINT uncheckColor);
	void SetConditionalText(const CString &checkedText, const CString &uncheckedText = {});
	void SetClickCallback(CWnd *receiver, clickCallbackType method, uint32_t clickdata = 0);

private:
	void TryUpdate();

protected:
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);

	DECLARE_MESSAGE_MAP()
};

#endif