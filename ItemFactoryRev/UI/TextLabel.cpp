
#include "stdafx.h"
#include "../stdafx.h"
#include "TextLabel.h"

TextLabel::TextLabel()
	: CStatic(), m_textColor(RGB(0, 0, 0))
{
	SetBackgroundColor(RGB(255, 255, 255));
}

void TextLabel::SetBackgroundColor(UINT rgb)
{
	m_bkColor = rgb;
	m_bkBrush = std::make_unique<CBrush>(m_bkColor);
}

void TextLabel::SetTextColor(UINT rgb)
{
	m_textColor = rgb;
}

BEGIN_MESSAGE_MAP(TextLabel, CStatic)
	ON_WM_CTLCOLOR_REFLECT()
END_MESSAGE_MAP()


HBRUSH TextLabel::CtlColor(CDC* pDC, UINT nCtlColor)
{
	pDC->SetTextColor(m_textColor);
	pDC->SetBkColor(m_bkColor);
	return *m_bkBrush;
}
