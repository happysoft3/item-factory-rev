
#include "stdafx.h"
#include "coreUi.h"
#include "itemFactoryLib.h"
#include "PageManager.h"
#include "ccobjectlib/GenericRoot.h"
#include "ccobjectlib\GenericData.h"

#include "utils\stringUtils.h"

#include "corePageManagerSignalsDef.h"
#include "pageUiSignalsDef.h"
#include <sstream>

CoreUi::CoreUi()
	: CCObject()
{
	m_mainWndCb = nullptr;
	m_mainWnd = nullptr;
	m_procConnectCount = 0;
	m_factory = FactoryInterface::MakeInstance();

	m_createCount = 1;
	m_amount = 20;
	m_posX = 700;
	m_posY = 700;
	m_invincible = false;
	m_createAtUnitPos = true;
	m_volatile = false;
}

CoreUi::~CoreUi()
{ }

void CoreUi::Initialize()
{
	CCObject *factoryLibObject = dynamic_cast<CCObject *>(m_factory.get());

	if (!factoryLibObject)	//@brief. 유효하지 않은 객체
		return;
	//@brief. 팩토리Lib 객체의 신호를 듣는다
	factoryLibObject->Connection(ItemFactoryLib::SignalDesc::START_COMMUNICATE, this, [](CCObject *receiver, std::shared_ptr<GenericRoot> msg) { dynamic_cast<CoreUi*>(receiver)->ReadyCommunicate(msg); });
	factoryLibObject->Connection(ItemFactoryLib::SignalDesc::REPORT_PROCESS_ALIVE, this, [](CCObject *receiver, std::shared_ptr<GenericRoot> msg) { dynamic_cast<CoreUi*>(receiver)->UpdateState(msg); });
	factoryLibObject->Connection(ItemFactoryLib::SignalDesc::RELEASE_ITEM_LIST, this,
		[](CCObject *receiver, std::shared_ptr<GenericRoot> msg) { dynamic_cast<CoreUi *>(receiver)->GetItemListFromLib(msg); });
	
	//@brief. 팩토리 객체가 CoreUi 객체의 1번 신호를 듣는다
	Connection(ItemFactoryLib::SignalDesc::START_COMMUNICATE, factoryLibObject, [](CCObject *receiver, std::shared_ptr<GenericRoot> msg) { dynamic_cast<ItemFactoryLib *>(receiver)->StartCommunicate(msg); });

	std::shared_ptr<GenericRoot> message(new GenericRoot());

	message->insert("Message", GenericData(this));

	Emit(ItemFactoryLib::SignalDesc::START_COMMUNICATE, message);
}

void CoreUi::RegistMainWndEventCallback(CWnd *sender, mainWndCbType eventCb)
{
	m_mainWnd = sender;
	m_mainWndCb = eventCb;
}

void CoreUi::SendMainWndMessage(UiEvent eventId, const std::string &message)
{
	if (m_mainWndCb != nullptr)
	{
		m_mainWndCb(m_mainWnd, eventId, message);
	}
}

void CoreUi::ReadyCommunicate(std::shared_ptr<GenericRoot> message)
{
	std::string msgContent = message->FirstKey();
	CCObject *factory = dynamic_cast<CCObject *>(m_factory.get());
	
	//@brief. 팩토리Lib 과 연결완료 된 시점, Ui 에 나머지 데이터를 요청함
	factory->Connection(ItemFactoryLib::SignalDesc::REPORT_ERROR, this,
		[](CCObject *r, std::shared_ptr<GenericRoot> m) { dynamic_cast<CoreUi *>(r)->ListenErrorFromLib(m); });
	factory->Connection(ItemFactoryLib::SignalDesc::REPORT_USER_POSITION, this,
		[](CCObject *r, std::shared_ptr<GenericRoot> m) { dynamic_cast<CoreUi *>(r)->ListenUserPosition(m); });
	factory->Connection(ItemFactoryLib::SignalDesc::SEND_DESCRIPT_DATA, this,
		[](CCObject *r, std::shared_ptr<GenericRoot> m) { r->Emit(CoreManagerSignals::CORE_RELEASE_DESC_DATA, m); });
}

void CoreUi::UpdateState(std::shared_ptr<GenericRoot> message)
{
	bool pResult = message->FirstValue().ToBool();

	if (pResult)
		++m_procConnectCount;
	SendMainWndMessage(UiEvent::NOTIFY_STATE, pResult ? "ok" : "ng");
}

//@brief. 에러나면 여기서 알려줌
void CoreUi::ListenErrorFromLib(std::shared_ptr<GenericRoot> report)
{
	auto keyIterator = report->Find("error_report");
	
	if (keyIterator != report->end())
	{
		std::string errorMessage = keyIterator->second.ToString();
		std::wstring displayMsg;

		StringUtils::AnsiToUnicode(errorMessage, displayMsg);
		::AfxMessageBox(CString(displayMsg.data()));
	}
}

//@brief. 성공 시 알려줌
void CoreUi::ListenCreateSuccess(std::shared_ptr<GenericRoot> result)
{ }

void CoreUi::RegistPageManager(std::shared_ptr<CCObject> pageManager)
{
	if (pageManager)
	{
		//@brief. 이 시점부터 PageManager에 연결된 페이지Ui 객체들이 CoreUi 와 다이렉트 통신이 시작됩니다
		m_pageManager = pageManager;

		//@brief. 수신부(PageManager->CoreUi)
		pageManager->Connection(CoreManagerSignals::TRY_CONNECTION, this,
			[](CCObject* r, std::shared_ptr<GenericRoot> m) { dynamic_cast<CoreUi *>(r)->TryConnection(m); });
		pageManager->Connection(CoreManagerSignals::REQUEST_LIST_UPDATE, this,
			[](CCObject *r, std::shared_ptr<GenericRoot> m) { dynamic_cast<CoreUi *>(r)->RequestUpdateList(m); });
		pageManager->Connection(CoreManagerSignals::REQUEST_ITEM_CREATE_FIN, this,
			[](CCObject *r, std::shared_ptr<GenericRoot> m) { dynamic_cast<CoreUi *>(r)->SendRequestCreateItem(m); });
		pageManager->Connection(CoreManagerSignals::REQUEST_CLIENT_SEND, this,
			[](CCObject *r, std::shared_ptr<GenericRoot> m) { dynamic_cast<CoreUi *>(r)->ClientCreateBuffer(m); });
		pageManager->Connection(CoreManagerSignals::REQUEST_SETTING_PARAMS, this,
			[](CCObject *r, std::shared_ptr<GenericRoot> m) { dynamic_cast<CoreUi *>(r)->SendSettingParams(m); });
		pageManager->Connection(CoreManagerSignals::ASYNC_COUNT_VALUE, this,
			[](CCObject *r, std::shared_ptr<GenericRoot> m) { dynamic_cast<CoreUi *>(r)->SetCreateCount(m); });
		pageManager->Connection(CoreManagerSignals::SYNC_CHECK_DATA, this,
			[](CCObject *r, std::shared_ptr<GenericRoot> m) { dynamic_cast<CoreUi *>(r)->SetBooleandata(m); });
		pageManager->Connection(CoreManagerSignals::REQUEST_DESCRIPT_DATA, this,
			[](CCObject *r, std::shared_ptr<GenericRoot> m) { r->Emit(ItemFactoryLib::SignalDesc::CORE_REQUEST_DESCIPT, m); });

		pageManager->Connection(CoreManagerSignals::REQUEST_UNIT_CREATE, this,
			[](CCObject *r, std::shared_ptr<GenericRoot> m) { dynamic_cast<CoreUi*>(r)->RequestCreateObject(m); });

		pageManager->Connection(CoreManagerSignals::SHOT_SETTING_DATA, this,
			[](CCObject *r, std::shared_ptr<GenericRoot> m) { dynamic_cast<CoreUi*>(r)->ReceiveSettingData(m); });

		//@brief. 송신부(CoreUi->PageManager)
		Connection(CoreManagerSignals::MANAGER_SET_SYNC, pageManager.get(),
			[](CCObject *r, std::shared_ptr<GenericRoot> m) { dynamic_cast<PageManager *>(r)->SetSyncValue(m); });
		Connection(CoreManagerSignals::CORE_SEND_ITEMLISTDATA, pageManager.get(),
			[](CCObject *r, std::shared_ptr<GenericRoot> m) { dynamic_cast<PageManager *>(r)->SendMessageToScreen("mainPage", PageSignals::MAINPAGE_UPDATELIST_DATA, m); });
		Connection(CoreManagerSignals::UPDATE_SETTING_PARAMS, pageManager.get(),
			[](CCObject *r, std::shared_ptr<GenericRoot> m) { dynamic_cast<PageManager *>(r)->UpdateSettingParams(m); });
		Connection(CoreManagerSignals::MANAGER_DO_ACTION, pageManager.get(),
			[](CCObject *r, std::shared_ptr<GenericRoot> m) { dynamic_cast<PageManager *>(r)->DoAction(m); });
		Connection(CoreManagerSignals::CORE_RELEASE_DESC_DATA, pageManager.get(),
			[](CCObject *r, std::shared_ptr<GenericRoot> m) { dynamic_cast<PageManager*>(r)->UpdateItemDescription(m); });

		Emit(CoreManagerSignals::MANAGER_DO_ACTION, std::shared_ptr<GenericRoot>(new GenericRoot({ std::make_pair("#action", GenericData("init")) })) );
	}
}

std::shared_ptr<CoreUi> CoreUi::MakeInstance()
{
	return std::make_shared<CoreUi>();
}

void CoreUi::TryConnection(std::shared_ptr<GenericRoot> message)
{
	ItemFactoryLib *lib = dynamic_cast<ItemFactoryLib *>(m_factory.get());

	lib->OpenTargetProcess(message);
}

void CoreUi::RequestUpdateList(std::shared_ptr<GenericRoot> message)
{
	if (m_procConnectCount ^ message->FirstValue().ToInt() && !m_pageManager.expired())
	{
		std::shared_ptr<GenericRoot> syncMsg(new GenericRoot());

		syncMsg->insert("syncUpdate", GenericData(static_cast<int>(m_procConnectCount)));
		syncMsg->insert("#pagename", GenericData("mainPage"));

		Emit(CoreManagerSignals::MANAGER_SET_SYNC, syncMsg);
		Emit(ItemFactoryLib::SignalDesc::REQUEST_ITEM_LIST, std::shared_ptr<GenericRoot>(new GenericRoot( { std::make_pair(std::string("update"), GenericData("Weapons")) })) );
		Emit(ItemFactoryLib::SignalDesc::REQUEST_ITEM_LIST, std::shared_ptr<GenericRoot>(new GenericRoot({ std::make_pair(std::string("update"), GenericData("Armors")) })));

		Emit(ItemFactoryLib::SignalDesc::REQUEST_ITEM_LIST, std::shared_ptr<GenericRoot>(new GenericRoot({ std::make_pair(std::string("update"), GenericData("QualityProperty")) })));
		Emit(ItemFactoryLib::SignalDesc::REQUEST_ITEM_LIST, std::shared_ptr<GenericRoot>(new GenericRoot({ std::make_pair(std::string("update"), GenericData("MaterialProperty")) })));
		Emit(ItemFactoryLib::SignalDesc::REQUEST_ITEM_LIST, std::shared_ptr<GenericRoot>(new GenericRoot({ std::make_pair(std::string("update"), GenericData("EnchantProperty")) })));
	}
}

void CoreUi::GetItemListFromLib(std::shared_ptr<GenericRoot> message)
{
	//@brief. 여기서 리스트를 받았음, Page매니저 에게 전달함

	Emit(CoreManagerSignals::CORE_SEND_ITEMLISTDATA, message);
}

//@brief. Page에서 받은 정보를 합산하여 아이템 생성 요청을 합니다
void CoreUi::SendRequestCreateItem(std::shared_ptr<GenericRoot> message)	//here	//Todo. 이 메시지를 매니저가 먼저 받는다
{
	message->insert("createCount", GenericData(m_createCount));
	message->insert("capacity", GenericData(m_amount));
	message->insert("x_position", GenericData(m_posX));
	message->insert("y_position", GenericData(m_posY));
	message->insert("invincible", GenericData(m_invincible));
	message->insert("atUserPos", GenericData(m_createAtUnitPos));
	message->insert("volatile", GenericData(m_volatile));
	//Todo. 여기에서 몇가지 더 추가필요

	Emit(ItemFactoryLib::SignalDesc::REQUEST_CREATEITEM, message);
}

void CoreUi::SendSettingParams(std::shared_ptr<GenericRoot> message)
{
	message->insert("x_position", GenericData(m_posX));
	message->insert("y_position", GenericData(m_posY));
	message->insert("createCount", GenericData(m_createCount));
	message->insert("capacity", GenericData(m_amount));
	message->insert("invincible", GenericData(m_invincible));
	message->insert("atUserPos", GenericData(m_createAtUnitPos));
	message->insert("volatile", GenericData(m_volatile));

	message->insert("#pagename", GenericData("settingPage"));

	Emit(CoreManagerSignals::UPDATE_SETTING_PARAMS, message);
}

void CoreUi::SetCreateCount(std::shared_ptr<GenericRoot> message)
{
	std::string countString = (*message)["createCount"].ToString();
	std::istringstream stream(countString);

	stream >> m_createCount;
}

void CoreUi::SetBooleandata(std::shared_ptr<GenericRoot> data)
{
	std::string type = (*data)["#checkType"].ToString();
	std::string checkData = (*data)["#checkData"].ToString();
	bool result = (!checkData.compare("true")) ? true : false;

	if (!type.compare("atUserPos"))
		m_createAtUnitPos = result;
	else if (!type.compare("invincible"))
		m_invincible = result;
	else if (!type.compare("volatile"))
		m_volatile = result;
}

void CoreUi::ListenUserPosition(std::shared_ptr<GenericRoot> report)
{
	if (m_createAtUnitPos)
	{
		int posX = (*report)["x_position"].ToInt();
		int posY = (*report)["y_position"].ToInt();

		m_posX = (posX > 0) ? posX : 700;
		m_posY = (posY > 0) ? posY : 700;
	}
}

void CoreUi::RequestCreateObject(std::shared_ptr<GenericRoot> request)
{
	request->insert("#xpos", GenericData(m_posX));
	request->insert("#ypos", GenericData(m_posY));
	request->insert("#userpos", GenericData(m_createAtUnitPos));

	Emit(ItemFactoryLib::SignalDesc::CORE_REQUEST_CREATEUNIT, request);
}

void CoreUi::ClientCreateBuffer(std::shared_ptr<GenericRoot> msg)
{
	Emit(ItemFactoryLib::CORE_REQUEST_USER_POSITION, msg);

	msg->insert("x_position", GenericData(m_posX));
	msg->insert("y_position", GenericData(m_posY));
	msg->insert("createCount", GenericData(m_createCount));
	msg->insert("capacity", GenericData(m_amount));
	msg->insert("invincible", GenericData(m_invincible));
	msg->insert("volatile", GenericData(m_volatile));

	Emit(ItemFactoryLib::CORE_REQUEST_CLIENTSEND, msg);
}

void CoreUi::ReceiveSettingData(std::shared_ptr<GenericRoot> msg)
{
	m_posX = (*msg)["x_position"].ToInt();
	m_posY = (*msg)["y_position"].ToInt();
	m_createCount = (*msg)["createCount"].ToInt() & 0xf;
	m_amount = (*msg)["capacity"].ToInt() & 0xff;
}