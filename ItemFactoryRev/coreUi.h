
#ifndef CORE_UI_H__
#define CORE_UI_H__

#include "ccobjectlib/CCObject.h"

#include <memory>
#include <string>
#include <functional>

class FactoryInterface;

class CoreUi : public CCObject
{
public:
	enum UiEvent
	{
		NOTIFY_STATE,
		CONNECTION_OK
	};
	using mainWndCbType = std::function<void(CWnd*, uint32_t, std::string)>;

private:
	std::unique_ptr<FactoryInterface> m_factory;
	mainWndCbType m_mainWndCb;
	CWnd *m_mainWnd;
	std::weak_ptr<CCObject> m_pageManager;
	uint32_t m_procConnectCount;

	int m_posX;
	int m_posY;
	int m_createCount;
	int m_amount;
	bool m_invincible;
	bool m_createAtUnitPos;
	bool m_volatile;
	
public:
	explicit CoreUi();
	CoreUi(const CoreUi &) = delete;
	CoreUi(CoreUi &&) noexcept = delete;
	virtual ~CoreUi() override;

	void Initialize();

private:
	void SendMainWndMessage(UiEvent eventId, const std::string &message);

public:
	void RegistMainWndEventCallback(CWnd *sender, mainWndCbType eventCb);

private:
	void ReadyCommunicate(std::shared_ptr<GenericRoot> message);
	void UpdateState(std::shared_ptr<GenericRoot> message);
	void ListenErrorFromLib(std::shared_ptr<GenericRoot> report);
	void ListenCreateSuccess(std::shared_ptr<GenericRoot> result);

public:
	void RegistPageManager(std::shared_ptr<CCObject> pageManager);
	static std::shared_ptr<CoreUi> MakeInstance();

private:
	void TryConnection(std::shared_ptr<GenericRoot> message);
	void RequestUpdateList(std::shared_ptr<GenericRoot> message);
	void GetItemListFromLib(std::shared_ptr<GenericRoot> message);
	void SendRequestCreateItem(std::shared_ptr<GenericRoot> message);
	void SendSettingParams(std::shared_ptr<GenericRoot> message);
	void SetCreateCount(std::shared_ptr<GenericRoot> message);

public:
	void SetBooleandata(std::shared_ptr<GenericRoot> data);
	void ListenUserPosition(std::shared_ptr<GenericRoot> report);

private:
	void RequestCreateObject(std::shared_ptr<GenericRoot> request);
	void ClientCreateBuffer(std::shared_ptr<GenericRoot> msg);
	void ReceiveSettingData(std::shared_ptr<GenericRoot> msg);
};

#endif