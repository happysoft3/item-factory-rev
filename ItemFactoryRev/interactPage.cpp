
#include "stdafx.h"
#include "interactPage.h"
#include "ccobjectlib\CCObject.h"
#include "ccobjectlib\GenericRoot.h"


InteractPage::InteractPage(UINT _nIDTemplate)
	: CTabPage(_nIDTemplate), m_eventCb(nullptr)
{ }

InteractPage::~InteractPage()
{ }

void InteractPage::SetManager(std::shared_ptr<CCObject> manager, InteractPage::pageEventCbType cb)
{
	m_manager = manager;
	m_eventCb = cb;
}

void InteractPage::ScreenMessage(uint32_t id, std::shared_ptr<GenericRoot> message)
{ }

void InteractPage::SetPagename(const std::string &pagename)
{
	m_pagename = pagename;
}

std::string InteractPage::Pagename() const
{
	return m_pagename;
}

void InteractPage::NotifyEvent(uint32_t eventId, const std::string &message)
{
	if (m_manager.expired() || m_eventCb == nullptr)
		return;

	std::shared_ptr<CCObject> obj = m_manager.lock();

	m_eventCb(obj.get(), eventId, message);
}

void InteractPage::SendToManager(uint32_t messageId, std::shared_ptr<GenericRoot> message)
{
	if (m_manager.expired())
		return;

	m_manager.lock()->Emit(messageId, message);
}