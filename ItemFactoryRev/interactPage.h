
#ifndef INTERACT_PAGE_H__
#define INTERACT_PAGE_H__

#include "UI\CTabPage.h"

#include <memory>
#include <functional>

class CCObject;
class GenericRoot;

class InteractPage : public CTabPage
{
public:
	using pageEventCbType = std::function<void(CCObject*, uint32_t, std::string)>;

private:
	std::weak_ptr<CCObject> m_manager;
	pageEventCbType m_eventCb;
	std::string m_pagename;

public:
	InteractPage(UINT _nIDTemplate);
	virtual ~InteractPage() override;

	void SetManager(std::shared_ptr<CCObject> manager, pageEventCbType cb);
	virtual void ScreenMessage(uint32_t id, std::shared_ptr<GenericRoot> message);
	void SetPagename(const std::string &pagename);
	std::string Pagename() const;

protected:
	void NotifyEvent(uint32_t eventId, const std::string &message);
	void SendToManager(uint32_t messageId, std::shared_ptr<GenericRoot> message);
};

#endif