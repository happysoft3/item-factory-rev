
#include "stdafx.h"
#include "resource.h"
#include "mainPage.h"
#include "ccobjectlib\CCObject.h"
#include "ccobjectlib\GenericRoot.h"
#include "ccobjectlib\GenericData.h"
#include "pageUiSignalsDef.h"

#include "corePageManagerSignalsDef.h"
#include "utils\stringUtils.h"

#include <algorithm>
#include <iterator>
#include <vector>

IMPLEMENT_DYNAMIC(MainPage, CTabPage)

MainPage::MainPage(UINT nIDTemplate)
	: InteractPage(nIDTemplate),
	m_itemListCtl(listId::ITEM_LIST),
	m_quailtyListCtl(listId::QUAILITY_LIST),
	m_materialListCtl(listId::MATERIAL_LIST),
	m_mainPropertyCtl(listId::MAIN_PROPERTY_LIST),
	m_subPropertyCtl(listId::SUB_PROPERTY_LIST)
{ }

MainPage::~MainPage()
{ }

void MainPage::OnInitialUpdate()
{
	CTabPage::OnInitialUpdate();

	m_listCtlMap[listId::ITEM_LIST] = std::make_tuple("itemname", &m_itemListCtl);
	m_listCtlMap[listId::QUAILITY_LIST] = std::make_tuple("quality", &m_quailtyListCtl);
	m_listCtlMap[listId::MATERIAL_LIST] = std::make_tuple("material", &m_materialListCtl);
	m_listCtlMap[listId::MAIN_PROPERTY_LIST] = std::make_tuple("magic1", &m_mainPropertyCtl);
	m_listCtlMap[listId::SUB_PROPERTY_LIST] = std::make_tuple("magic2", &m_subPropertyCtl);
}

void MainPage::InitCControls()
{
	auto ButtonStyleGuide = [](CMFCButton &cButton, const CString &caption)
	{
		cButton.SetTextColor(RGB(255, 255, 255));
		cButton.SetFaceColor(RGB(64, 128, 128));
		cButton.SetWindowTextW(caption);
	};
	auto SubDropListStyleGuide = [](CDropdownList &listCtl)
	{
		listCtl.ChangeBackgroundColor(RGB(60, 98, 130));
		listCtl.ChangeTextColor(RGB(253, 210, 231));
	};
	m_createButton.EnableWindowsTheming(FALSE);

	m_itemLabel.SetWindowTextW(L"아이템");
	m_magicLabel.SetWindowTextW(L"프로퍼티");

	ButtonStyleGuide(m_createButton, CString(L"아이템생성"));
	ButtonStyleGuide(m_clientCreateButton, CString(L"클라이언트 생성"));

	m_itemListCtl.SetCallback(this, &MainPage::DropdownCallback);
	m_quailtyListCtl.SetCallback(this, &MainPage::DropdownCallback);
	m_materialListCtl.SetCallback(this, &MainPage::DropdownCallback);
	m_mainPropertyCtl.SetCallback(this, &MainPage::DropdownCallback);
	m_subPropertyCtl.SetCallback(this, &MainPage::DropdownCallback);

	m_itemListCtl.ChangeBackgroundColor(RGB(85, 172, 143));
	m_itemListCtl.ChangeTextColor(RGB(232, 255, 233));

	SubDropListStyleGuide(m_quailtyListCtl);
	SubDropListStyleGuide(m_materialListCtl);
	SubDropListStyleGuide(m_mainPropertyCtl);
	SubDropListStyleGuide(m_subPropertyCtl);

	m_descText.SetWindowTextW(L"최근 선택된 항목설명: ");
	m_descText.SetTextColor(RGB(32, 95, 96));
	m_descDisplay.SetTextColor(RGB(69, 31, 97));
}

void MainPage::OnEnterScreen()
{
	m_descDisplay.SetWindowTextW({});
	NotifyEvent(PageSignals::MAINPAGE_UPDATELIST, "requestUpdate");
}

//콤보박스 선택 시 처리
void MainPage::DropdownListEventProc(uint32_t id, const CString &message)
{
	std::string dest;
	StringUtils::UnicodeToAnsi(std::wstring(message.operator LPCWSTR()), dest);
	std::shared_ptr<GenericRoot> request(new GenericRoot);

	request->insert("#listId", GenericData(static_cast<int>(id)));
	switch (id)
	{
	case listId::ITEM_LIST:
		request->insert("#key", GenericData("items"));
		break;
	case listId::QUAILITY_LIST:
		request->insert("#key", GenericData("QualityProperty"));
		break;
	case listId::MATERIAL_LIST:
		request->insert("#key", GenericData("MaterialProperty"));
		break;
	case listId::MAIN_PROPERTY_LIST:
	case listId::SUB_PROPERTY_LIST:
		request->insert("#key", GenericData("EnchantProperty"));
		break;
	}
	request->insert("#value", GenericData(dest));
	request->insert("#pagename", GenericData(Pagename()));

	//request->Insert("#ctl_id", GenericData(static_cast<int>(id)));					//TODO. 콤보박스 선택 시 한글설명 업데이트 하려고 했더니,
	//request->Insert("#type", GenericData(std::get<0>(m_listCtlMap[id])));			//Lib이랑 ui 랑 키값이 다르다.. 어케 해결?? 아무튼 FIXME
	//request->Insert("#key", GenericData(dest));
	//request->Insert("#pagename", GenericData("mainPage"));
	SendToManager(CoreManagerSignals::REQUEST_DESCRIPT_DATA, request);
	//std::cout << "!RemoveMe! void MainPage::DropdownListEventProc(uint32_t id, const CString &message)" << std::endl;
}

void MainPage::DropdownCallback(CWnd *wnd, uint32_t id, CString message)
{
	MainPage *page = dynamic_cast<MainPage *>(wnd);

	if (page != nullptr)
		page->DropdownListEventProc(id, message);
}

void MainPage::ClearAllListCtl()
{
	m_itemListCtl.Clear();
	m_quailtyListCtl.Clear();
	m_materialListCtl.Clear();
	m_mainPropertyCtl.Clear();
	m_subPropertyCtl.Clear();
}

void MainPage::ListUpdateProc(std::shared_ptr<GenericRoot> message)
{
	std::list<std::string> itemList;
	std::string messageKey("#itemTypeId");
	uint32_t listTypeId = (*message)[messageKey].ToInt();

	message->Remove(messageKey);
	std::transform(message->begin(), message->end(), std::insert_iterator<std::list<std::string>>(itemList, itemList.begin()),
		[](const auto &data)->std::string { return data.second.ToString(); });
	std::vector<CDropdownList *> listCtlV;
	bool useNone = true;

	switch (listTypeId)
	{
	case messageId::MSG_ARMOR_LIST:
	case messageId::MSG_WEAPON_LIST:
		listCtlV.push_back(&m_itemListCtl);
		useNone = false;
		break;
	case messageId::MSG_QUAILITY_LIST:
		listCtlV.push_back(&m_quailtyListCtl);
		break;
	case messageId::MSG_MATERIAL_LIST:
		listCtlV.push_back(&m_materialListCtl);
		break;
	case messageId::MSG_PROPERTY_LIST:
		listCtlV.push_back(&m_mainPropertyCtl);
		listCtlV.push_back(&m_subPropertyCtl);
		break;
	case messageId::MSG_UNDEFINED:
	default:
		return;
	}
	if (useNone && itemList.size())
		itemList.push_front(std::string("None"));
	std::for_each(listCtlV.begin(), listCtlV.end(), [&itemList](CDropdownList *listComponent) { listComponent->UpdateItemList(itemList); });
}

void MainPage::ScreenMessage(uint32_t id, std::shared_ptr<GenericRoot> message)
{
	switch (id)
	{
	case PageSignals::MAINPAGE_UPDATELIST_DATA:
		ListUpdateProc(message);
		break;
	case PageSignals::MAINPAGE_CLEAR_ALL_LIST:
		ClearAllListCtl();
		break;
	case PageSignals::MAINPAGE_UPDATE_DESCRIPT:
		DisplaySelectedDescription((*message)["#desc"].ToString());
		break;
	}
}

std::shared_ptr<GenericRoot> MainPage::MakeSelectItemMessage()
{
	std::shared_ptr<GenericRoot> itemMsg(new GenericRoot);

	std::transform(m_listCtlMap.begin(), m_listCtlMap.end(), std::insert_iterator<GenericRoot>(*itemMsg, itemMsg->begin()),
		[](const auto &item) { return std::make_pair(std::get<0>(item.second), GenericData(std::get<1>(item.second)->GetSelectItemName())); });
	return itemMsg;
}

void MainPage::DisplaySelectedDescription(const std::string &desc)
{
	std::wstring wdest;
	StringUtils::AnsiToUnicode(desc, wdest);

	m_descDisplay.SetWindowTextW(CString(wdest.data()));
}

void MainPage::DoDataExchange(CDataExchange *pDX)
{
	CTabPage::DoDataExchange(pDX);

	DDX_Control(pDX, WORKSET_ITEM_TEXT, m_itemLabel);
	DDX_Control(pDX, WORKSET_MAGIC_TEXT, m_magicLabel);
	DDX_Control(pDX, WORKSET_BUTTON1, m_createButton);
	DDX_Control(pDX, WORKSET_BUTTON2, m_clientCreateButton);

	DDX_Control(pDX, WORKSET_COMBO_TOP, m_itemListCtl);
	DDX_Control(pDX, WORKSET_COMBO1, m_quailtyListCtl);
	DDX_Control(pDX, WORKSET_COMBO2, m_materialListCtl);
	DDX_Control(pDX, WORKSET_COMBO3, m_mainPropertyCtl);
	DDX_Control(pDX, WORKSET_COMBO4, m_subPropertyCtl);

	DDX_Control(pDX, WORKSET_DESC_TEXT, m_descText);
	DDX_Control(pDX, WORKSET_DESC_TEXT2, m_descDisplay);

	InitCControls();
}

void MainPage::ClickCreateItem()
{
	std::shared_ptr<GenericRoot> itemmsg = std::move(MakeSelectItemMessage());

	//itemmsg->insert("#pagename", GenericData("settingPage"));
	//itemmsg->insert("#notify", GenericData(static_cast<int>(PageSignals::SETTINGPAGE_REQUEST_DATA)));

	SendToManager(CoreManagerSignals::REQUEST_ITEM_CREATE_FIN, itemmsg);
}

void MainPage::ClickClientCreateItem()
{
	std::shared_ptr<GenericRoot> itemmsg = std::move(MakeSelectItemMessage());

	SendToManager(CoreManagerSignals::REQUEST_CLIENT_SEND, itemmsg);
}

BEGIN_MESSAGE_MAP(MainPage, CTabPage)
	ON_BN_CLICKED(WORKSET_BUTTON1, ClickCreateItem)
	ON_BN_CLICKED(WORKSET_BUTTON2, ClickClientCreateItem)
END_MESSAGE_MAP()
