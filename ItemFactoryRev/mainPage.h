
#ifndef MAIN_PAGE_H__
#define MAIN_PAGE_H__

#include "interactPage.h"
#include "UI/TextLabel.h"
#include "UI\CDropdownList.h"

#include <map>

class GenericRoot;

//@brief. 메인 페이지 구성입니다
class MainPage : public InteractPage
{
	DECLARE_DYNAMIC(MainPage)

private:
	TextLabel m_itemLabel;
	TextLabel m_magicLabel;
	CMFCButton m_createButton;
	CMFCButton m_clientCreateButton;
	CDropdownList m_itemListCtl;
	CDropdownList m_quailtyListCtl;
	CDropdownList m_materialListCtl;
	CDropdownList m_mainPropertyCtl;
	CDropdownList m_subPropertyCtl;
	using ctlElement = std::tuple<std::string, CDropdownList*>;
	std::map<uint32_t, ctlElement> m_listCtlMap;

	TextLabel m_descText;
	TextLabel m_descDisplay;

	enum listId
	{
		ITEM_LIST,
		QUAILITY_LIST,
		MATERIAL_LIST,
		MAIN_PROPERTY_LIST,
		SUB_PROPERTY_LIST
	};

	enum messageId
	{
		MSG_UNDEFINED,
		MSG_ARMOR_LIST,
		MSG_WEAPON_LIST,
		MSG_QUAILITY_LIST,
		MSG_MATERIAL_LIST,
		MSG_PROPERTY_LIST
	};

public:
	explicit MainPage(UINT nIDTemplate);
	virtual ~MainPage() override;

	virtual void OnInitialUpdate() override;

private:
	void InitCControls();
	virtual void OnEnterScreen() override;

	void DropdownListEventProc(uint32_t id, const CString &message);
	static void DropdownCallback(CWnd *wnd, uint32_t id, CString message);

	void ClearAllListCtl();
	void ListUpdateProc(std::shared_ptr<GenericRoot> message);
	virtual void ScreenMessage(uint32_t id, std::shared_ptr<GenericRoot> message) override;
	std::shared_ptr<GenericRoot> MakeSelectItemMessage();
	void DisplaySelectedDescription(const std::string &desc);

protected:
	virtual void DoDataExchange(CDataExchange *pDX);
	afx_msg void ClickCreateItem();
	afx_msg void ClickClientCreateItem();
	DECLARE_MESSAGE_MAP()
};

#endif