//{{NO_DEPENDENCIES}}
// Microsoft Visual C++에서 생성한 포함 파일입니다.
// ItemFactoryRev.rc에서 사용되고 있습니다.
//
#define IDM_ABOUTBOX                    0x0010
#define IDD_ABOUTBOX                    100
#define IDS_ABOUTBOX                    101
#define IDD_FORMVIEW                    101
#define IDD_ITEMFACTORYREV_DIALOG       102
#define IDD_FORMVIEW1                   103
#define IDD_FORMVIEW2                   104
#define IDR_MAINFRAME                   138
#define IDB_PNG1                        139
#define IDB_PNG2                        140
#define IDB_BITMAP1                     141
#define IDB_PNG3                        142
#define IDB_PNG4                        143
#define IDB_PNG5                        144
#define IDB_PNG6                        145
#define IDB_PNG7                        146
#define IDB_PNG8                        147
#define IDB_PNG9                        148
#define IDB_PNG10                       149
#define IDB_PNG11                       150
#define IDB_PNG12                       151
#define MAINWND_LOADER                  1000
#define MAINWND_BUTTON1                 1001
#define MAINWND_BUTTON2                 1002
#define WORKSET_ITEM_TEXT               1002
#define WORKSET_MAGIC_TEXT              1003
#define MAINWND_BUTTON3                 1003
#define WORKSET_COMBO_TOP               1004
#define WORKSET_BUTTON1                 1005
#define SETTING_CHECK1                  1006
#define WORKSET_DESC_TEXT               1006
#define WORKSET_COMBO1                  1007
#define SETTING_CHECK2                  1007
#define WORKSET_COMBO2                  1008
#define SETTING_GROUP                   1008
#define WORKSET_COMBO3                  1009
#define SETTING_INPUT1                  1009
#define WORKSET_COMBO4                  1010
#define SETTING_TEXT1                   1010
#define WORKSET_BUTTON2                 1011
#define SETTING_INPUT2                  1011
#define SETTING_TEXT2                   1012
#define WORKSET_DESC_TEXT2              1012
#define SETTING_TEXT3                   1013
#define SETTING_TEXT4                   1014
#define SETTING_INPUT3                  1015
#define SETTING_INPUT4                  1016
#define SETTING_BUTTON1                 1017
#define SETTING_BUTTON2                 1018
#define CREATOR_INPUT                   1018
#define SETTING_BUTTON3                 1019
#define CREATOR_BUTTON1                 1019
#define SETTING_TEXT5                   1020
#define CREATOR_TEXT1                   1020
#define MAINWND_STAT_TEXT               1021
#define CREATOR_UNIT_BUTTON1            1021
#define SETTING_CHECK3                  1021
#define CREATOR_UNIT_BUTTON2            1022
#define CREATOR_UNIT_BUTTON3            1023
#define CREATOR_TASKGROUP               1024
#define CREATOR_UNIT_BUTTON4            1026
#define CREATOR_UNIT_BUTTON5            1027
#define CREATOR_UNIT_BUTTON6            1028
#define CREATOR_UNIT_BUTTON7            1029
#define CREATOR_UNIT_BUTTON8            1030
#define CREATOR_UNIT_BUTTON9            1031
#define CREATOR_UNIT_BUTTON10           1032
#define CREATOR_UNIT_BUTTON11           1033
#define CREATOR_UNIT_BUTTON12           1034

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        152
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1025
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
