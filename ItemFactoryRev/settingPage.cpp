
#include "stdafx.h"
#include "resource.h"
#include "settingPage.h"
#include "pageUiSignalsDef.h"

#include "ccobjectlib\GenericRoot.h"
#include "ccobjectlib\GenericData.h"

#include "corePageManagerSignalsDef.h"

IMPLEMENT_DYNAMIC(SettingPage, CTabPage)

SettingPage::SettingPage(UINT nIDTemplate)
	: InteractPage(nIDTemplate)
{
	m_checkedColor = RGB(0, 255, 0);
	m_uncheckedColor = RGB(237, 28, 36);
}

SettingPage::~SettingPage()
{ }

void SettingPage::OnInitialUpdate()
{
	CTabPage::OnInitialUpdate();
}

void SettingPage::InitCControls()
{
	auto ButtonStyleGuide = [](CMFCButton &cButton, const CString &caption)
	{
		cButton.SetTextColor(RGB(255, 255, 255));
		cButton.SetFaceColor(RGB(64, 128, 128));
		cButton.SetWindowTextW(caption);
	};
	m_xCoorText.SetWindowTextW(L"X좌표");
	m_yCoorText.SetWindowTextW(L"Y좌표");
	m_amountText.SetWindowTextW(L"소비류 수량조절\n(1-255)");
	m_countText.SetWindowTextW(L"생성 아이템 개수: ");
	m_connectText.SetWindowTextW(L"작업 전 꼭 눌러주세요!");
	m_connectText.SetTextColor(RGB(225, 32, 92));

	GetDlgItem(SETTING_GROUP)->SetWindowTextW(L"아이템 생성 위치설정");

	ButtonStyleGuide(m_connectButton, L"프로세스 연결");
	ButtonStyleGuide(m_minusButton, L"[-]");
	ButtonStyleGuide(m_plusButton, L"[+]");

	m_xCoorInput.SetTextColor(RGB(255, 0, 0));
	m_yCoorInput.SetTextColor(RGB(255, 0, 0));
	m_xCoorInput.SetLimitText(4);
	m_yCoorInput.SetLimitText(4);
	m_countInput.SetReadOnly(TRUE);

	m_amountInput.SetLimitText(3);

	m_userPosCheck.SetConditionalText(L"캐릭터 위치에 생성");
	m_userPosCheck.SetConditionalTextColor(m_checkedColor, m_uncheckedColor);
	m_userPosCheck.SetConditionalBkColor(RGB(112, 146, 190), RGB(176, 115, 187));
	m_userPosCheck.SetClickCallback(this, [](CWnd *r, uint32_t id, bool state)
		{ dynamic_cast<SettingPage *>(r)->CheckBoxStateChanged(id, state); }, PageSignals::SETTINGPAGE_USER_POS_CHECK);

	m_invCheck.SetConditionalTextColor(m_checkedColor, m_uncheckedColor);
	m_invCheck.SetConditionalText(L"파괴불가 아이템");
	m_invCheck.SetConditionalBkColor(RGB(112, 146, 190), RGB(176, 115, 187));
	m_invCheck.SetClickCallback(this, [](CWnd *r, uint32_t id, bool state)
		{ dynamic_cast<SettingPage *>(r)->CheckBoxStateChanged(id, state); }, PageSignals::SETTINGPAGE_INVINCIBLE_CHECK);

	m_volatileCheck.SetConditionalTextColor(m_checkedColor, m_uncheckedColor);
	m_volatileCheck.SetConditionalText(L"휘발성 오브젝트로 생성");
	m_volatileCheck.SetConditionalBkColor(RGB(112, 146, 190), RGB(176, 115, 187));
	m_volatileCheck.SetClickCallback(this, [](CWnd *r, uint32_t id, bool state)
	{ dynamic_cast<SettingPage *>(r)->CheckBoxStateChanged(id, state); }, PageSignals::SETTINGPAGE_VOLATILE_CHECK);
}

void SettingPage::OnEnterScreen()
{
	NotifyEvent(PageSignals::SETTINGPAGE_UPDATE_REQUEST, "requestUpdate");
}

void SettingPage::OnExitScreen()
{
	ShotSettingData();
}

void SettingPage::ShowSettingParams(std::shared_ptr<GenericRoot> message)
{
	m_xCoorInput.SetWindowTextNumber((*message)["x_position"].ToInt());
	m_yCoorInput.SetWindowTextNumber((*message)["y_position"].ToInt());

	m_countInput.SetWindowTextNumber((*message)["createCount"].ToInt());
	m_amountInput.SetWindowTextNumber((*message)["capacity"].ToInt());

	bool invcheck = (*message)["invincible"].ToBool();
	bool userposcheck = (*message)["atUserPos"].ToBool();
	bool isVolatile = (*message)["volatile"].ToBool();

	m_invCheck.SetCheckStatus(invcheck ? TRUE : FALSE);
	m_userPosCheck.SetCheckStatus(userposcheck ? TRUE : FALSE);
	m_volatileCheck.SetCheckStatus(isVolatile ? TRUE : FALSE);
}

void SettingPage::SendPageData(std::shared_ptr<GenericRoot> msg, uint32_t signal)	//here
{
	auto validChecker = [](int n) { return (n >= 100 && n < 5000) ? n : 500; };

	msg->insert("x_position", GenericData(m_xCoorInput.GetWindowTextNumber(validChecker)));
	msg->insert("y_position", GenericData(m_yCoorInput.GetWindowTextNumber(validChecker)));

	msg->insert("createCount", GenericData(m_countInput.GetWindowTextNumber()));
	msg->insert("capacity", GenericData(m_amountInput.GetWindowTextNumber()));

	SendToManager(signal, msg);
}

void SettingPage::ScreenMessage(uint32_t id, std::shared_ptr<GenericRoot> message)
{
	switch (id)
	{
	case PageSignals::SETTINGPAGE_UPDATE_PARAM:
		ShowSettingParams(message);
		break;

	case PageSignals::SETTINGPAGE_REQUEST_DATA:
		SendPageData(message, CoreManagerSignals::REQUEST_ITEM_CREATE_FIN);
		break;

	case PageSignals::MAINPAGE_CLIENT_SEND:
		SendPageData(message, CoreManagerSignals::REQUEST_CLIENT_SEND);
		break;
	}
}

void SettingPage::SetCreateCount(std::function<int(int)> mathExpr)
{
	CString content;

	m_countInput.GetWindowTextW(content);
	int number = mathExpr(_ttoi(content));
	
	NotifyEvent(PageSignals::SETTINGPAGE_COUNT_AYSNC_REQUEST, std::to_string(number));
	m_countInput.SetWindowTextNumber(number);
}

void SettingPage::CheckBoxStateChanged(uint32_t notifyId, bool state)
{
	SetFocus();

	std::shared_ptr<GenericRoot> update(new GenericRoot);
	
	switch (notifyId)
	{
	case PageSignals::SETTINGPAGE_USER_POS_CHECK:
		update->insert("#checkType", GenericData("atUserPos"));
		break;
	case PageSignals::SETTINGPAGE_INVINCIBLE_CHECK:
		update->insert("#checkType", GenericData("invincible"));
		break;
	case PageSignals::SETTINGPAGE_VOLATILE_CHECK:
		update->insert("#checkType", GenericData("volatile"));
	}
	update->insert("#checkData", GenericData(state ? "true" : "false"));
	SendToManager(CoreManagerSignals::SYNC_CHECK_DATA, update);
}

void SettingPage::ShotSettingData()
{
	auto validChecker = [](int n) { return (n >= 100 && n < 5750) ? n : 700; };
	std::shared_ptr<GenericRoot> shot(new GenericRoot);

	shot->insert("x_position", GenericData(m_xCoorInput.GetWindowTextNumber(validChecker)));
	shot->insert("y_position", GenericData(m_yCoorInput.GetWindowTextNumber(validChecker)));

	shot->insert("createCount", GenericData(m_countInput.GetWindowTextNumber() & 0xf));

	int amount = m_amountInput.GetWindowTextNumber() & 0xff;

	shot->insert("capacity", GenericData((amount == 0) ? 20 : amount));

	SendToManager(CoreManagerSignals::SHOT_SETTING_DATA, shot);
}

void SettingPage::DoDataExchange(CDataExchange *pDX)
{
	CTabPage::DoDataExchange(pDX);

	DDX_Control(pDX, SETTING_TEXT1, m_xCoorText);
	DDX_Control(pDX, SETTING_TEXT2, m_yCoorText);
	DDX_Control(pDX, SETTING_TEXT3, m_countText);
	DDX_Control(pDX, SETTING_TEXT4, m_amountText);
	DDX_Control(pDX, SETTING_TEXT5, m_connectText);
	DDX_Control(pDX, SETTING_BUTTON1, m_minusButton);
	DDX_Control(pDX, SETTING_BUTTON2, m_plusButton);
	DDX_Control(pDX, SETTING_BUTTON3, m_connectButton);

	DDX_Control(pDX, SETTING_INPUT1, m_xCoorInput);
	DDX_Control(pDX, SETTING_INPUT2, m_yCoorInput);
	DDX_Control(pDX, SETTING_INPUT3, m_countInput);
	DDX_Control(pDX, SETTING_INPUT4, m_amountInput);
	
	DDX_Control(pDX, SETTING_CHECK1, m_userPosCheck);
	DDX_Control(pDX, SETTING_CHECK2, m_invCheck);
	DDX_Control(pDX, SETTING_CHECK3, m_volatileCheck);

	InitCControls();
}

void SettingPage::ClickCountMinus()
{
	SetCreateCount([](int x)->int { return (x - 1 > 0) ? (x - 1) : x; });
}

void SettingPage::ClickCountPlus()
{
	SetCreateCount([](int x)->int { return (x + 1 < 10) ? (x + 1) : x; });
}

void SettingPage::ClickConnect()
{
	NotifyEvent(PageSignals::SETTING_CONNECT, "tryConnection");
}

BEGIN_MESSAGE_MAP(SettingPage, CTabPage)
	ON_BN_CLICKED(SETTING_BUTTON3, ClickConnect)
	ON_BN_CLICKED(SETTING_BUTTON1, ClickCountMinus)
	ON_BN_CLICKED(SETTING_BUTTON2, ClickCountPlus)
END_MESSAGE_MAP()

