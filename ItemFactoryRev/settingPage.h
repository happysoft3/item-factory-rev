
#ifndef SETTING_PAGE_H__
#define SETTING_PAGE_H__

#include "interactPage.h"
#include "UI\TextLabel.h"
#include "UI\TextInput.h"
#include "UI/CheckButton.h"

class SettingPage : public InteractPage
{
	DECLARE_DYNAMIC(SettingPage)

private:
	CMFCButton m_connectButton;
	CMFCButton m_minusButton;
	CMFCButton m_plusButton;
	TextLabel m_xCoorText;
	TextLabel m_yCoorText;
	TextLabel m_countText;
	TextLabel m_amountText;
	TextLabel m_connectText;
	TextInput m_xCoorInput;
	TextInput m_yCoorInput;
	TextInput m_countInput;
	TextInput m_amountInput;
	CheckButton m_invCheck;
	CheckButton m_userPosCheck;
	CheckButton m_volatileCheck;

private:
	UINT m_checkedColor;
	UINT m_uncheckedColor;

public:
	explicit SettingPage(UINT nIDTemplate);
	virtual ~SettingPage();

	virtual void OnInitialUpdate() override;

private:
	void InitCControls();

	virtual void OnEnterScreen() override;
	virtual void OnExitScreen() override;
	void ShowSettingParams(std::shared_ptr<GenericRoot> message);
	void SendPageData(std::shared_ptr<GenericRoot> msg, uint32_t signal);
	virtual void ScreenMessage(uint32_t id, std::shared_ptr<GenericRoot> message) override;
	void SetCreateCount(std::function<int(int)> mathExpr);
	void CheckBoxStateChanged(uint32_t notifyId, bool state);

	void ShotSettingData();

protected:
	virtual void DoDataExchange(CDataExchange *pDX);
	afx_msg void ClickCountMinus();
	afx_msg void ClickCountPlus();
	afx_msg void ClickConnect();
	DECLARE_MESSAGE_MAP()
};

#endif