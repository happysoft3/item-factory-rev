
#include "stdafx.h"
#include "resource.h"
#include "subPage.h"

#include "ccobjectlib\GenericRoot.h"
#include "ccobjectlib\GenericData.h"

#include "category.h"

#include "corePageManagerSignalsDef.h"
#include "utils\stringUtils.h"

#include <algorithm>
#include <iterator>
#include <string>

IMPLEMENT_DYNAMIC(SubPage, CTabPage)

SubPage::SubPage(UINT nIDTemplate)
	: InteractPage(nIDTemplate), m_category(new Category)
{ }

SubPage::~SubPage()
{ }

void SubPage::OnInitialUpdate()
{
	CTabPage::OnInitialUpdate();

	m_category->InsertNewItem("explosion", { "BlackPowderBarrel", "BlackPowderBarrel2" });
	m_category->InsertNewItem("workset", { "WizardWorkstation1", "WizardWorkstation1b", "WizardWorkstation2a", "WizardWorkstation2b", "WizardWorkstation3d", "WizardWorkstation2d" });
	m_category->InsertNewItem("germs", { "Ruby", "Emerald", "Diamond" });
	m_category->InsertNewItem("container", {"TargetBarrel1", "TargetBarrel2", "Barrel", "Barrel2", "BarrelLOTD", "Crate1", "Crate2", "DarkCrate1", "DarkCrate2"});
}

void SubPage::OnExitScreen()
{
	m_inputItem.Empty();
}

void SubPage::InitCControls()
{
	auto ButtonStyleGuide = [](CMFCButton &cButton, const CString &caption)
	{
		cButton.SetTextColor(RGB(255, 255, 255));
		cButton.SetFaceColor(RGB(64, 128, 128));
		cButton.SetWindowTextW(caption);
	};
	m_runButton.EnableWindowsTheming(FALSE);
	m_titleText.SetWindowTextW(L"유닛 생성기능");
	m_titleText.SetTextColor(RGB(255, 0, 225));

	ButtonStyleGuide(m_runButton, L"유닛소환");

	m_inputItem.SetLimitText(30);
	m_inputItem.SetTextColor(RGB(0, 0, 255));

	m_unitButton1.UpdateImage(IDB_PNG1);
	m_unitButton1.SetClickCallback(this, [](CWnd *r) { dynamic_cast<SubPage*>(r)->UnitItemSelected("RedPotion"); });
	m_unitButton1.SetWindowTextW(L"체력회복약");

	m_unitButton2.UpdateImage(IDB_PNG3);
	m_unitButton2.SetClickCallback(this, [](CWnd *r) { dynamic_cast<SubPage*>(r)->UnitItemSelected("BluePotion"); });
	m_unitButton2.SetWindowTextW(L"마나회복약");

	m_unitButton3.UpdateImage(IDB_PNG4);
	m_unitButton3.SetClickCallback(this, [](CWnd *r) { dynamic_cast<SubPage*>(r)->UnitItemSelected("CurePoisonPotion"); });
	m_unitButton3.SetWindowTextW(L"해독제");

	m_unitButton11.UpdateImage(IDB_PNG2);
	m_unitButton11.SetClickCallback(this, [](CWnd *r) { dynamic_cast<SubPage*>(r)->UnitItemSelected("ShieldPotion"); });
	m_unitButton11.SetWindowTextW(L"쉴드포션");

	m_unitButton12.UpdateImage(IDB_PNG5);
	m_unitButton12.SetClickCallback(this, [](CWnd *r) { dynamic_cast<SubPage*>(r)->UnitItemSelected("SilverKey"); });
	m_unitButton12.SetWindowTextW(L"실버열쇠");

	m_unitButton13.UpdateImage(IDB_PNG6);
	m_unitButton13.SetClickCallback(this, [](CWnd *r) { dynamic_cast<SubPage*>(r)->UnitItemSelected("GoldKey"); });
	m_unitButton13.SetWindowTextW(L"골드열쇠");

	m_unitButton21.UpdateImage(IDB_PNG7);
	m_unitButton21.SetClickCallback(this, [](CWnd *r) { dynamic_cast<SubPage*>(r)->UnitItemSelected("Horrendous"); });
	m_unitButton21.SetWindowTextW(L"호렌더스");

	m_unitButton22.UpdateImage(IDB_PNG8);
	m_unitButton22.SetClickCallback(this, [](CWnd *r) { dynamic_cast<SubPage*>(r)->SelectFromCategory("explosion"); });
	m_unitButton22.SetWindowTextW(L"폭발물");

	m_unitButton23.UpdateImage(IDB_PNG9);
	m_unitButton23.SetClickCallback(this, [](CWnd *r) { dynamic_cast<SubPage*>(r)->SelectFromCategory("workset"); });
	m_unitButton23.SetWindowTextW(L"실험도구");

	m_unitButton31.UpdateImage(IDB_PNG12);
	m_unitButton31.SetClickCallback(this, [](CWnd *r) { dynamic_cast<SubPage*>(r)->UnitItemSelected("AnkhTradable"); });
	m_unitButton31.SetWindowTextW(L"앵크");

	m_unitButton32.UpdateImage(IDB_PNG11);
	m_unitButton32.SetClickCallback(this, [](CWnd *r) { dynamic_cast<SubPage*>(r)->SelectFromCategory("container"); });
	m_unitButton32.SetWindowTextW(L"컨테이너");

	m_unitButton33.UpdateImage(IDB_PNG10);
	m_unitButton33.SetClickCallback(this, [](CWnd *r) { dynamic_cast<SubPage*>(r)->SelectFromCategory("germs"); });
	m_unitButton33.SetWindowTextW(L"보석류");

	GetDlgItem(CREATOR_TASKGROUP)->SetWindowTextW(L"빠른 작업목록");
}

void SubPage::SelectFromCategory(const std::string &categoryKey)
{
	UnitItemSelected(m_category->PickupItemAtRandom(categoryKey));
}

void SubPage::UnitItemSelected(const std::string &unitname)
{
	std::wstring dest;

	std::transform(unitname.begin(), unitname.end(), std::insert_iterator<std::wstring>(dest, dest.begin()), [](char byt) -> wchar_t { return byt; });
	m_inputItem.SetWindowTextW(CString(dest.data()));
}

void SubPage::DoDataExchange(CDataExchange *pDX)
{
	CTabPage::DoDataExchange(pDX);

	DDX_Control(pDX, CREATOR_TEXT1, m_titleText);
	DDX_Control(pDX, CREATOR_INPUT, m_inputItem);
	DDX_Control(pDX, CREATOR_BUTTON1, m_runButton);
	DDX_Control(pDX, CREATOR_UNIT_BUTTON1, m_unitButton1);
	DDX_Control(pDX, CREATOR_UNIT_BUTTON2, m_unitButton2);
	DDX_Control(pDX, CREATOR_UNIT_BUTTON3, m_unitButton3);
	DDX_Control(pDX, CREATOR_UNIT_BUTTON4, m_unitButton11);
	DDX_Control(pDX, CREATOR_UNIT_BUTTON5, m_unitButton12);
	DDX_Control(pDX, CREATOR_UNIT_BUTTON6, m_unitButton13);
	DDX_Control(pDX, CREATOR_UNIT_BUTTON7, m_unitButton21);
	DDX_Control(pDX, CREATOR_UNIT_BUTTON8, m_unitButton22);
	DDX_Control(pDX, CREATOR_UNIT_BUTTON9, m_unitButton23);
	DDX_Control(pDX, CREATOR_UNIT_BUTTON10, m_unitButton31);
	DDX_Control(pDX, CREATOR_UNIT_BUTTON11, m_unitButton32);
	DDX_Control(pDX, CREATOR_UNIT_BUTTON12, m_unitButton33);

	InitCControls();
}

void SubPage::ClickCreateUnitButton()
{
	CString input;
	std::string dest;

	m_inputItem.GetWindowText(input);
	m_inputItem.Empty();
	std::wstring wstr(input.operator LPCWSTR());

	std::transform(wstr.begin(), wstr.end(), std::insert_iterator<std::string>(dest, dest.begin()), [](uint16_t word)->char { return static_cast<char>(word); });
	std::shared_ptr<GenericRoot> request(new GenericRoot);

	request->insert("#pagename", GenericData(Pagename()));
	request->insert("#data", GenericData(dest));
	SendToManager(CoreManagerSignals::REQUEST_UNIT_CREATE, request);
}

BEGIN_MESSAGE_MAP(SubPage, CTabPage)
	ON_BN_CLICKED(CREATOR_BUTTON1, ClickCreateUnitButton)
END_MESSAGE_MAP()