
#ifndef SUB_PAGE_H__
#define SUB_PAGE_H__

#include "interactPage.h"
#include "UI\TextLabel.h"
#include "UI\TextInput.h"
#include "UI/ImageButton.h"

class Category;

class SubPage : public InteractPage
{
	DECLARE_DYNAMIC(SubPage)

private:
	TextLabel m_titleText;
	TextInput m_inputItem;
	CMFCButton m_runButton;
	ImageButton m_unitButton1;
	ImageButton m_unitButton2;
	ImageButton m_unitButton3;
	ImageButton m_unitButton11;
	ImageButton m_unitButton12;
	ImageButton m_unitButton13;
	ImageButton m_unitButton21;
	ImageButton m_unitButton22;
	ImageButton m_unitButton23;
	ImageButton m_unitButton31;
	ImageButton m_unitButton32;
	ImageButton m_unitButton33;
	std::unique_ptr<Category> m_category;

public:
	explicit SubPage(UINT nIDTemplate);
	virtual ~SubPage() override;

	virtual void OnInitialUpdate() override;

private:
	virtual void OnExitScreen() override;
	void InitCControls();
	void SelectFromCategory(const std::string &categoryKey);
	void UnitItemSelected(const std::string &unitname);

protected:
	virtual void DoDataExchange(CDataExchange *pDX);
	afx_msg void ClickCreateUnitButton();
	DECLARE_MESSAGE_MAP()
};

#endif